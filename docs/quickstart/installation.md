# 安装上手

### npm 包安装方法

> npm i hc-basic -S

## 基础安装

> 使用默认的 hcBasic 配置内容，不做任何修改定制化

```vue
main.js（入口文件）： import hcBasic from 'hc-basic'; import '~hc-basic/packages/scss/basic'; Vue.use(hcBasic);
```

## 自定义主题安装

<quickstart-theme></quickstart-theme>

## 自定义项目配置安装

<quickstart-config></quickstart-config>
