# 项目目录配置

```
├── README.md
├── babel.config.js
├── package-lock.json
├── package.json
├── public
│   ├── favicon.ico
│   └── index.html
├── src
│   ├── App.vue                     ------>    项目VUE主文件
│   ├── assets                      ------>    静态资源存放目录
│   │   ├── image                   ------>    图片
│   │   └── sass                    ------>    项目样式
│   │       ├── index.scss          ------>    scss入口文件
│   │       ├── hcConfig.scss      ------>    hc-basic npm包配置文件
│   │       ├── resetElement.scss   ------>    项目Element样式重置
│   │       ├── common.scss         ------>    项目公共样式
│   │       ├── module.scss         ------>    项目scss模块封装
│   │       ├── config.scss         ------>    项目scss变量配置
│   │       └── pages               ------>    页面项目scss文件
│   ├── components                  ------>    项目公共组件
│   ├── http                        ------>    axiosHttp请求配置
│   │   ├── axios.config.js         ------>    默认axios参数配置
│   │   ├── index.js                ------>    axios拦截器配置、私有配置
│   │   └── service                 ------>    http接口URL地址统一管理
│   │       ├── index.js            ------>    入口文件
│   │       └── system.js           ------>    项目模块配置
│   ├── main.js                     ------>    项目入口js文件
│   ├── router                      ------>    项目路由配置
│   │   ├── blocks                  ------>    路由参数文件
│   │   │   ├── index.js            ------>    入口文件
│   │   │   └── system.js           ------>    项目模块配置
│   │   ├── index.js                ------>    vue-router配置文件
│   │   └── menu                    ------>    菜单配置
│   │       ├── index.js            ------>    入口文件
│   │       └── system.js           ------>    项目模块配置
│   ├── store                       ------>    vueX
│   │   └── index.js                ------>    VUEX配置文件
│   ├── util                        ------>    vue项目工具
│   │   ├── common.js               ------>    公共处理
│   │   ├── mixins                  ------>    全局mixins
│   │   └── utils.js                ------>    插件配置
│   └── views                       ------>    页面文件
│       ├── home                    ------>    首页
│       ├── login                   ------>    登录页面
│       └── system                  ------>    项目模块文件夹
└── vue.config.js


```
