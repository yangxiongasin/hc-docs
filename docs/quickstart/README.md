# 示例项目

## 单体项目
>
[演示地址](http://114.115.150.172/sampleSingle)
>
[代码地址](https://codeup.aliyun.com/6180d6736746bc7c6cc8e78a/hc/sample-single.git)

## qiankun微前端项目
>
[演示地址](http://114.115.150.172:8082)
>
[主项目代码地址](https://codeup.aliyun.com/6180d6736746bc7c6cc8e78a/hc/sample-more-main.git)
>
[子项目代码地址](https://codeup.aliyun.com/6180d6736746bc7c6cc8e78a/hc/sample-more-child.git)
