# eslint 配置及自动化

## 项目中使用 eslint 的必要性说明

> 1.保持统一编码规范和统一的代码风格； 2.审查代码是否存在语法错误； 3.提高代码可读性及可维护性

## eslint 配置

<quickstart-eslint></quickstart-eslint>

## prettier-eslint-cli 插件

> prettier-eslint-cli 插件可以结合编辑器对代码根据设定对 eslin 规则进行格式化的修改

### 安装方法：

<quickstart-prettier-eslint></quickstart-prettier-eslint>
