# 介绍

:::tip
hc-basic 是一个基于 Vue + ElementUI 编写的一个项目内容公共包
:::

#### 1.elementUI 组件的扩展、二次封装

<div class="ml-20 text-14 color-text-regular">
<div class="mt-12">1.1对element UI内组件进行二次封装，降低组件使用难度<br/></div>
<div class="ml-20 mt-12">1.1.1对比示例：table组件使用方法<br/></div>
<div class="mt-12">1.2对element UI不包含对组件进行扩展，降低二次封装成本<br/></div>
</div>

#### 2.全局样式

<div class="ml-20 text-14 color-text-regular">
<div class="mt-12">2.1 公共基础样式封装<br/></div>
<div class="mt-12">2.2 通过config.scss文件进行主题的配置<br/></div>
</div>

#### 3.全局指令

> 使用方法与 VUE 过滤器一致

#### 4.全局公共函数

> 介绍： 数组、对象、存储、日期等项目开发常用函数集合，并且所有方法已挂载到 VUE 原型上

<div class="ml-20 text-14 color-text-regular">
<div>5.1数组方法集合<br/></div>
<div class="mt-12">5.2对象方法集合<br/></div>
<div class="mt-12">5.3日期方法集合<br/></div>
</div>

#### 5 项目常用正则

> 收集常用正则，通过存到 VUE 原型对象上，减少用户查询正则时间
