# Cookies

::: tip
通过"this.\$Func.方法名"使用
:::

## delCookie - 删除 Cookies

> this.\$Func.delCookie();

```js
const delCookie = (name) => {
    setCookie(name, "", "-1");
    // 通过建立 cookie 的时间设置，将时间设置提前一天，从而强行让 cookie 失效，最后达到 删除cookie 的目的
};
```

## getCookie - 取 cookie

> this.\$Func.delCookie();

```js
const getCookie = (name) => {
    let cookieArr = document.cookie.split(";");
    for (let i = 0; i < cookieArr.length; i++) {
        let arr = cookieArr[i].split("=");
        if (name === arr[0]) {
            return arr[1];
        }
    }
    return false;
};
```

## setCookie - 存 cookie

> this.\$Func.delCookie();

```js
const setCookie = (name, value, times) => {
    const date = new Date();
    date.setDate(date.getDate() + times);
    document.cookie = name + "=" + value + ";expires=" + date;
};
```
