# Array

::: tip
通过"this.\$Func.方法名"使用
:::

## 去除数组字段

<comcontent-array-filterOptionArray></comcontent-array-filterOptionArray>

## 数组扁平化

<comcontent-array-flatten></comcontent-array-flatten>

## getEleCount - 统计

<comcontent-array-getEleCount></comcontent-array-getEleCount>

## 判定是否为数组

<comcontent-array-isArray></comcontent-array-isArray>

## 判定是否为空数组

<comcontent-array-isEmptyArray></comcontent-array-isEmptyArray>

## 对象元素去重

<comcontent-array-repeatObjEle></comcontent-array-repeatObjEle>

## 去重

<comcontent-array-unSet></comcontent-array-unSet>
