# 使用指南

## 介绍

:::tip
hc-basic 是一个基于 Vue + ElementUI 编写的一个项目内容公共包

hc-basic 中包含了 - 全局过滤器 - 全局指令 - 项目常用正则 - 数组方法集合 - 对象方法集合 - 日期方法集合
:::

## 使用

#### 全局指令使用

> hc-basic 指令已通过全局注册，可直接使用

```vue
<img src="" v-errimg>
```

#### 全局正则使用

> hc-basic 正则存放在 Vue 原型上的\$Valid 对象中

```js
this.$Valid.MobilePhoneNumber;
```

#### 数组、对象、日期、cookies 方法使用

> hc-basic 公共方法存放在 VUE 原型上的\$Func 对象中

```js
this.$Func.cloneDeep({});
this.$Func.getCookies();
```
