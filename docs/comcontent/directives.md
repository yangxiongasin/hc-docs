# directives 自定义指令

---

::: tip
通过"v-指令名"使用
:::

## 防抖指令

<comcontent-directives-prevent-shake></comcontent-directives-prevent-shake>

#### prevent-shake => API

<common-api :api-data="[
        { params: 'value', describe: '再次点击生效时间', type: 'Number', defaultValue: '2000' }
    ]
"></common-api>

## 图片错误处理指令

<comcontent-directives-errimg></comcontent-directives-errimg>

#### errimg => API

<common-api :api-data="[
        { params: 'type', describe: 'error图片类型', type: 'String', defaultValue: 'showErr' },
        { params: 'empty', describe: '为空的时候是否需要展示err图片', type: 'Boolean', defaultValue: 'true' }
    ]
"></common-api>

## 复制指令

<comcontent-directives-copy></comcontent-directives-copy>

#### copy => API

<common-api :api-data="[
        { params: 'value', describe: '复制的内容', type: 'data', defaultValue: '' },
    ]
"></common-api>
