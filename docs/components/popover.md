# popover 弹出框

::: tip
elementUI 组件二次封装，将 popover 功能分类处理，减少开发成本
:::

### 基本 popover 用法

<comp-popover-basic></comp-popover-basic>

### popover 其他配置

<comp-popover-config></comp-popover-config>

<common-api :api-data="
	[
		{
	    params: 'title',
	    describe: 'popover显示的文字信息',
	    type: 'String',
	    defaultValue: ''
	  },
		{
      params: 'icon',
      describe: ' 图标样式',
      type: 'String',
      defaultValue: 'el-icon-warning-outline'
    },
    {
      params: 'class-name',
      describe: ' 设置图标样式',
      type: 'String',
      defaultValue: 'color-warning text-20'
    }
	]
"></common-api>

<common-api type='sd' :api-data="
	[
		{
			params: 'submit',
			describe: '点击确定后触发函数',
			cbparams: '',
		}
	]
"></common-api>
