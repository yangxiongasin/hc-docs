# frame 布局组件

::: tip
集成 menu 和 tabs 组件，主要使用场景为后台管理系统主体框架内容
:::

## 基础用法

<comp-frame-basic></comp-frame-basic>

## 菜单和叶签联动

## API

<common-api :api-data="[
    { params: 'collapse', describe: '菜单是否收起', type: 'Boolean', defaultValue: 'false' },
    { params: 'menu', describe: '是否显示菜单', type: 'Boolean', defaultValue: 'false' },
    { params: 'menuConfig', describe: '菜单数据映射配置, 详情见下方', type: 'Object', defaultValue: '{....}' },
    { params: 'config', describe: '组件各个模块宽高配置, 详情见下方', type: 'Object', defaultValue: '{....}' },
]
"></common-api>

### config

<common-api :api-data="[
    { params: 'width', describe: 'openMenu：菜单展开宽度， closeMenu：菜单收起宽度', type: 'Object', defaultValue: '{ openMenu: 240, closeMenu: 60 }' },
    { params: 'height', describe: 'header: 头部内容高度、logo: 头部Logo高度、footer：菜单底部高度、tabs: 页签高度', type: 'String', defaultValue: '{ header: 50, footer: 0, tabs: 50, logo: 50 }' },
]
"></common-api>

### menuConfig

<common-api :api-data="[
    { params: 'label', describe: '菜单名称', type: 'String', defaultValue: 'label' },
    { params: 'value', describe: '菜单唯一标志', type: 'String', defaultValue: 'value' },
    { params: 'icon', describe: '菜单图标配置', type: 'String', defaultValue: '' },
    { params: 'children', describe: '子级菜单数据字段', type: 'String', defaultValue: 'children' },
    { params: 'fullPath', describe: '当前菜单对应路由的全路径', type: 'String', defaultValue: '' },
]
"></common-api>

## Event

<common-api :api-data="
	[
		{
			params: 'change',
			describe: '页签选中状态变化时触发',
			cbparams: '当前选中状态页签值',
		}
	]
"></common-api>

## slots

<common-api :api-data="
	[
	    {  params: 'default',
            describe: '页面路由嵌入主内容位置'
    	},
		{params: 'logo',
			describe: '用来插入logo图片位置，config.height.logo为0时不显示'
		},
		{params: 'nav',
			describe: '顶部内容区域, config.height.header为0时不显示'
		},
		{params: 'footer',
			describe: '菜单下方预留位置'
		}
	]
"></common-api>
