# formItem 组件

---

## formItem 组件描述

::: tip
formItem 组件只处理 form 内部内容，需要包含在 el-form 组件内<br/>
支持 input、select、checkbox、radio、date-picker、switch 子组件、以及提供了单项的 slot 插槽
:::

## 基础用法

<comp-formItem-basic></comp-formItem-basic>

## rightSide

<comp-formItem-rightSide></comp-formItem-rightSide>

## Column

<comp-formItem-column></comp-formItem-column>

## Slot 嵌入

<comp-formItem-slot></comp-formItem-slot>

## 其他配置

<comp-formItem-other></comp-formItem-other>

## API

<common-api :api-data="[
    { params: 'form', describe: 'el-form表单总数据，跟el-form组件:model绑定的值保持一致', type: 'Object', defaultValue: '{}' },
    { params: 'rules', describe: '跟el-form组件:rules绑定的值保持一致', type: 'Object', defaultValue: '{}' },
    { params: 'config', describe: '组件的配置项，详细描述见configAPI', type: 'Object', defaultValue: '{}' },
    { params: 'column', describe: '每一行显示的列数，可选值（1，2，3，4，6）', type: 'Number', defaultValue: 4 },
    { params: 'right-side', describe: 'rightSide属性可将默认插槽内容放置在组件末尾', type: 'Boolean', defaultValue: 'false' },
    { params: 'same-label', describe: '设置所有form-item项表单label字段宽度自动计算，与labelWidth互斥', type: 'Boolean', defaultValue: 'false' },
    { params: 'width', describe: '设置所有form-item项表单内容宽度，支持单项设置', type: 'String', defaultValue: '100%' },
    { params: 'disabled', describe: '设置所有form-item表单项禁用，支持单项设置', type: 'Boolean', defaultValue: 'false' },
    { params: 'clearable', describe: '设置所有form-item表单项都带有clearable，支持单项设置', type: 'Boolean', defaultValue: 'false' },
    { params: 'label-width', describe: '设置所有form-item表单项LabelWidth宽度，与sameLabel互斥', type: 'String', defaultValue: '' },
    { params: 'no-label', describe: '将label字段不显示在form-item前方，放置于form-item项内', type: 'Boolean', defaultValue: 'false' },
]
"></common-api>

## configAPI

<common-api :api-data="[
    { params: 'itemType', describe: '渲染对应对组件： input、select、checkbox、radio、date-picker、switch、text', type: 'String', defaultValue: 'input' },
    { params: 'prop', describe: '对应数据绑定的字段', type: 'String', defaultValue: '--' },
    { params: 'label', describe: '对应数据绑定的字段', type: 'String', defaultValue: '--' },
    { params: 'span', describe: '占据form-item组件对应的span值等份', type: 'Number', defaultValue: 1 },
    { params: 'disabled', describe: '当前项禁用', type: 'Boolean', defaultValue: false },
    { params: 'clearable', describe: '当前项可清除', type: 'Boolean', defaultValue: false },
    { params: 'isSlot', describe: '当前项使用自定义slot内容', type: 'Boolean', defaultValue: false },
    { params: 'width', describe: '当前项表单项宽度', type: 'String', defaultValue: '' },
    { params: 'code', describe: '当前项字典值，值由后端提供', type: 'String', defaultValue: '' },
    { params: 'formatter', describe: '当前项表单值计算处理，入参为当前form的:model绑定值', type: 'Function', defaultValue: '' },
]
"></common-api>

## 全量配置示例

<comp-formItem-demo></comp-formItem-demo>
