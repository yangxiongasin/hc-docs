# 使用指南

## 介绍

::: tip
hc-basic 是一个基于 Vue + ElementUI 编写的一个项目内容公共包<br/>
hc-basic 中包含了 elementUI 组件的二次封装
:::

## 使用

> 内部定义的所有的组件均已注册为全局组件，添加了"hc-"为组件前缀名

#### 项目中使用

```vue
<hc-pager :total="total" :page="pager" layout="total, prev, pager, next"></hc-pager>
<hc-table :data="tableData" :column="tableColumn" :cellClassName="cellClassName" @onSelect="$comSelectData"> </hc-table>
<hc-form-item :form="ruleForm" :config="resetFormConfig"></hc-form-item>
```
