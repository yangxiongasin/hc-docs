# theme 主题组件

::: tip
页面换肤功能的实现
:::

## 基础用法

<comp-theme-basic></comp-theme-basic>

## other 入参使用示例

<comp-theme-other></comp-theme-other>

## API

<common-api :api-data="[
{ params: 'functional', describe: '主题色配置', type: 'Object', defaultValue: '{ primary: #409eff, success: #67c23a, info: #909399, warning: #e6a23c, danger: #f56c6c }' },
{ params: 'text', describe: '文本颜色', type: 'Object', defaultValue: '{ primary: #303133, regular: #606266, secondary: #909399, placeholder: #c0c4cc }' },
{ params: 'border', describe: '边框颜色', type: 'Object', defaultValue: '{ base: #dcdfe6, light: #e4e7ed, lighter: #ebeef5, extraLight: #f2f6fc }' },
{ params: 'shadow', describe: '阴影颜色', type: 'String', defaultValue: '#000000' },
{ params: 'other', describe: '其他颜色配置', type: 'Object', defaultValue: '{}' },
]
"></common-api>
