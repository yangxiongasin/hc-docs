# Desc 详情组件

::: tip
组件扩展，该组件主要使用于详情页面
:::

## 基本 Desc 用法

<comp-desc-basic></comp-desc-basic>

## 自定义 slot 插槽

<comp-desc-slot></comp-desc-slot>

## 设置 Desc 样式

<comp-desc-style></comp-desc-style>

## API 参数

<common-api :api-data="[
        { params: 'data', describe: '传入组件对应的数据', type: 'Object', defaultValue: '{}' },
        { params: 'config', describe: '数据列的配置描述，具体项及使用方法参考示例代码', type: 'Array', defaultValue: '[]' },
        { params: 'column', describe: '设置每一行显示的数据列数，可选值（1，2，3，4，6）', type: 'Number, String', defaultValue: 4 },
        { params: 'sameLabel', describe: '设置所有desc项label字段宽度自动计算', type: 'Boolean', defaultValue: 'false' },
        { params: 'color',describe: '设置所有desc项label和value颜色，可选值（primary,success,danger,info,warning,textPrimary,regular,secondary,placeholder,white）', type:'Object', defaultValue: '{}'  },
        { params: 'distance',describe: '设置所有desc项行间距（4的倍数）', type:'Number, String', defaultValue: '16' },
    ]
"></common-api>

### config 参数

<common-api :api-data="[
        { params: 'prop', describe: '对应数据绑定的字段', type: 'String', defaultValue: '--' },
        { params: 'label', describe: '对应数据的label值', type: 'String', defaultValue: '--' },
        { params: 'span', describe: '对应数据占据组件的span值等份', type: 'Number', defaultValue: 1 },
        { params: 'code', describe: '对应数据绑定字段的字典回显名称', type: 'String', defaultValue: '（无回显）' },
        { params: 'formatter', describe: '列数据的计算方法', type: 'Function', defaultValue: '入参为data' },
        { params: 'isSlot', describe: '是否需要对该列进行slot自定义，slotName为当列对应的prop值', type: 'Boolean', defaultValue: 'false' },
        { params: 'click', describe: '为当前列增加点击事件', type: 'Function', defaultValue: '入参为当前数据' }
    ]
"></common-api>
