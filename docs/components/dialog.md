# Dialog 组件

::: tip
elementUI 组件二次封装，主要将 dialog 显示隐藏内置化，并扩展自适应高度、可拖拽等功能
:::

## 基础用法

<comp-dialog-basic></comp-dialog-basic>

## 打开前回调

<comp-dialog-beforeOpen></comp-dialog-beforeOpen>

## single 全屏

<comp-dialog-single></comp-dialog-single>

## 可拖拽

<comp-dialog-drag></comp-dialog-drag>

## API

<common-api :api-data="
    [
        { params: 'outClose', describe: '弹窗打开关闭', type: 'Boolean', defaultValue: false },
        { params: 'single', describe: 'dialog高度是否占据屏幕，超出部分内容区域滚动', type: 'Boolean', defaultValue: false },
        { params: 'drag', describe: '是否开启弹窗拖拽功能', type: 'Boolean', defaultValue: false },
    ]
"></common-api>
