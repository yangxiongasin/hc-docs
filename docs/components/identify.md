# Identify 组件

::: tip
验证码组件封装，主要使用在登录页面场景<br/>
登录时常用验证码组件，支持点击刷新验证码，自定义验证码字段长度、字段内容，自定义宽高
:::

## 基础用法

<comp-identify-basic></comp-identify-basic>

## 自定义长度

<comp-identify-length></comp-identify-length>

## 自定义内容

<comp-identify-content></comp-identify-content>

## 自定义宽高

<comp-identify-wh></comp-identify-wh>

## API

<common-api :api-data="[
    { params: 'value', describe: '组件绑定的值', type: 'String', defaultValue: '' },
    { params: 'codeLength', describe: '验证码长度', type: 'String\Number', defaultValue: '4' },
    { params: 'composition', describe: '验证码随机出现字符', type: 'String', defaultValue: '1234567890ABCDEFGHIGKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' },
    { params: 'width', describe: '宽度', type: 'Number', defaultValue: 90 },
    { params: 'height', describe: '高度', type: 'Number', defaultValue:38 },
]
"></common-api>

## 全量配置示例

<comp-identify-demo></comp-identify-demo>
