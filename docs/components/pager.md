# Pager 分页组件

---

::: tip
pager 组件二次封装，将 pager 组件 pageSize 改变、currentPage 改变，归于 change 事件抛出，参数为当前的 pager 数据
:::

## 基本 pager 用法

<comp-pager-basic></comp-pager-basic>

## 自定义 layout Pager 用法

<comp-pager-layout></comp-pager-layout>

## API

<common-api :api-data="[
        { params: 'total', describe: 'pager总数', type: 'Number', defaultValue: '0' },
        { params: 'page', describe: 'pager数据重置回显需要， 需要设置为页面绑定的pager值', type: 'Object', defaultValue: '{}' },
        { params: 'layout', describe: '组件布局，子组件名用逗号分隔', type: 'String', defaultValue: 'total, prev, pager, next, sizes, jumper' }
    ]
"></common-api>

## Event

<common-api :api-data="
	[
		{
			params: 'change',
			describe: 'pageSize、currentPage  改变时会触发，入参为当前组件pager值',
			cbparams: 'page对象',
		}
	]
"></common-api>
