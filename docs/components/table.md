# table 组件

::: tip
针对 element UI 的 table 组件进行二次封装以及"合计"等功能扩展
:::

## 基础用法

<comp-tables-basic></comp-tables-basic>

## 操作列

<comp-tables-operation></comp-tables-operation>

## 设置单元格样式

<comp-tables-cellConfig></comp-tables-cellConfig>

## 多选功能及数据

<comp-tables-checkbox></comp-tables-checkbox>

## 自定义列、自定义表头

<comp-tables-columnSlot></comp-tables-columnSlot>

## 展开内容

<comp-tables-expend></comp-tables-expend>

## API

<common-api :api-data="[
    { params: 'data', describe: '显示的数据', type: 'Array', defaultValue: '[]' },
    { params: 'column', describe: '表格列的配置描述，具体项及使用方法参考示例代码', type: 'Array', defaultValue: '[]' },
    { params: 'expand', describe: '是否显示展开列', type: 'Boolean', defaultValue: 'false' },
    { params: 'showFixed', describe: '是否让左右两列浮动，中间内容滚动', type: 'Boolean', defaultValue: 'true' },
    { params: 'checkbox', describe: '是否显示多选框', type: 'Boolean', defaultValue: 'false' },
    { params: 'operationWidth', describe: '操作项宽度, 有值且非`auto`时操作列自动展示', type: 'String', defaultValue: 'auto' },
    { params: 'cellConfig', describe: '为所有单元格设置一个固定的 className，与cellClassName互斥', type: 'Object', defaultValue: '「」' },
    {
        params: 'cellClassName',
        describe: '单元格的 className 的回调方法，也可以使用字符串为所有单元格设置一个固定的 className。',
        type: 'Function({row, column, rowIndex, columnIndex})',
        defaultValue: '----'
    },
    { params: 'showHeaders', describe: '自定义表头的slots name数组集合', type: 'Array', defaultValue: '[]' },
]
"></common-api>

### column API

<common-api :api-data="[
    { params: 'prop', describe: '当前列的key字段', type: 'String', defaultValue: '--' },
    { params: 'label', describe: '当前列的label值', type: 'String', defaultValue: '--' },
    { params: 'isSlot', describe: '设置该列是否为slot插入', type: 'Boolean', defaultValue: 'false' },
    { params: 'code', describe: '当该列需要通过字典值回显的时候设置，值为后端提供的code值, 与formatter互斥', type: 'String、Number', defaultValue: '' },
    { params: 'formatter', describe: '格式化指定列的值，接受一个Function，会传入一个参数：row', type: 'Function', defaultValue: '--' },
]
"></common-api>

## Event

<common-api type='sd' :api-data="
	[
		{params: 'on-select', describe: '当用户手动勾选或全选时数据行的 Checkbox 时触发的事件,入参为当前选中的数据集合数组', cbparams: 'selection'}
	]
"></common-api>
