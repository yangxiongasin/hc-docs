# Icon 组件

::: tip
使用 SVG Sprites 进行字体图标管理。

> 目标：为了解决 font-class 字体每次新增图标时都需要全部重新安装的痛点
> :::

## vue.config.js 配置

<comp-icon-first></comp-icon-first>

## svg 极其对应配置

<comp-icon-second></comp-icon-second>

## 组件用法

<comp-icon-user></comp-icon-user>

## API

<common-api :api-data="
    [
        { params: 'name', describe: '设定使用哪个svg对应的svg图标', type: 'String', defaultValue: '' },
        { params: 'type', describe: '设置icon图标的颜色，属性值对应文本颜色（primary、success、danger、info、warning、regular、secondary、placeholder、white）', type: 'Boolean', defaultValue: null },
        { params: 'size', describe: '设置icon的宽高属性', type: 'String|Number', defaultValue: '100%' }
    ]
"></common-api>
