---
home: true
actionText: 快速上手 →
heroImage: /image/home.jpg
actionLink: /quickstart/
features:
    - title: 公共样式
      details: 将开发页面的公共样式进行抽离，减少页面私有样式的编写，保持css代码统一性和高维护性
    - title: 公共组件
      details: 抽离页面多次使用的页面结构或组件形式。增加页面编辑速度，提高开发效率
    - title: 公共内容
      details: 提供公共过滤正则、公共函数等
footer: 维护： yweiaimin（微信） 1908825931@qq.com(邮箱)
---
