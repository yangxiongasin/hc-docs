/**
 * 扩展 VuePress 应用
 */
import VueHighlightJS from "vue-highlight.js";
import "highlight.js/styles/atom-one-dark.css";
import "../.vuepress/public/scss/index.scss";
import hcBasic from "hc-basic";

export default async ({
    Vue,
    options, // the options for the root Vue instance
    router, // the router instance for the app
    siteData, // site metadata,
    isServer
}) => {
    // ...做一些其他的应用级别的优化
    Vue.use(VueHighlightJS);
    // 使用 gui
    if (!isServer) {
        await import("../../node_modules/hc-basic/lib/hc-basic.umd.min").then((module) => {
            Vue.use(module.default);
        });
    }
};
