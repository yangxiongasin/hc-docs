module.exports = {
    title: "HC-Basic",
    description: "基于VUE + Element UI搭建的basic npm 包， 降低项目搭建、开发成本",
    dest: "../package/docs/dist",
    port: "8000",
    base: "/",
    markdown: {
        lineNumbers: true
    },
    // 注入到当前页面的 HTML <head> 中的标签
    head: [
        [
            "link",
            {
                rel: "icon",
                href: "/image/favicon.ico"
            }
        ] // 增加一个自定义的 favicon(网页标签的图标)
    ],
    themeConfig: {
        sidebarDepth: 2, // e'b将同时提取markdown中h2 和 h3 标题，显示在侧边栏上。
        lastUpdated: "Last Updated", // 文档更新时间：每个文件git最后提交的时间
        nav: [
            {
                text: "主页",
                link: "/"
            },
            {
                text: "快速上手",
                link: "/quickstart/"
            },
            {
                text: "样式元素",
                link: "/className/"
            },
            {
                text: "公共组件",
                link: "/components/"
            },
            {
                text: "公共内容",
                link: "/comcontent/"
            },
            {
                text: "技术分享",
                link: "/share/"
            }
            // 下拉列表的配置
            // {
            //   text: 'Languages',
            //   items: [
            //     { text: 'Chinese', link: '/language/chinese' },
            //     { text: 'English', link: '/language/English' }
            //   ]
            // }
        ],
        configureWebpack: {
            css: {
                modules: false,
                localIdentName: "[name]_[local]_[hash:base64:5]",
                sourceMap: false,
                loaderOptions: {
                    sass: {
                        implementation: require("sass")
                    }
                }
            }
        },
        sidebar: {
            "/quickstart/": [
                "",
                {
                    title: "快速上手",
                    collapsable: false,
                    children: ["introduce", "installation", "file", "eslint"]
                }
            ],
            "/className/": [
                "",
                {
                    title: "Block",
                    collapsable: false,
                    children: ["border", "cursor", "display", "flex", "float", "position", "space", "shadow"]
                },
                {
                    title: "Text",
                    collapsable: false,
                    children: ["typography", "bgColor"]
                },
                {
                    title: "Other",
                    collapsable: false,
                    children: ["Other"]
                }
            ],
            "/components/": ["", "frame", "table", "formItem", "dialog", "pager", "popover", "desc", "popover", "icon", "identify", "theme"],
            "/comcontent/": [
                "",
                "directives",
                "valid",
                {
                    title: "公共函数",
                    collapsable: false,
                    children: ["Array", "Cookies", "Date", "Object"]
                }
            ],
            "/share/": [
                "",
                "formTablePager",
                "multiProjectDeployment",
                "storeSelect",
                {
                    title: "qiankun微前端",
                    collapsable: false,
                    children: ["qiankun", "qiankunMain", "qiankunChild", "qiankunNginx", "qiankunSkill"]
                },
                "Observable"
            ]
        }
    }
};
