const code = {};
code.basic = `
<template>
    <hc-frame :data="menuData" :collapse="collapse" :menu-config="menuConfig" :config="frameConfig" menu>
        <template #logo>
            <div @click="$router.push('home')" class="bg-default flex-center border-bottom cursor-pointer" style="height: 53px; color: #4c93ff;">
                <span v-show="!collapse" class="flex-y-center">
                    <img src="../../assets/images/logo.png" style="width: 100%; height: 52px;" alt="">
                </span>
                <span v-show="collapse">
                    <img src="../../assets/images/logo2.png" style="width: 45px; height: 45px;" alt="">
                </span>
            </div>
        </template>
        <template #nav>
            <div class="bg-default flex-between-center color-info pl-16 pr-20 border-bottom shadow-basic" style="height: 53px;">
                <div class="d-flex">
                    <div @click="collapse = !collapse">
                        <div class="text-16 cursor-pointer">
                            <i v-show="!collapse" class="el-icon-s-fold" />
                            <i v-show="collapse" class="el-icon-s-unfold" />
                        </div>
                    </div>
                    <div class="pl-20 border-left ml-16 text-16" style="color: #262e3a;">
                        {{ title }}
                    </div>
                </div>
                <div class="flex-y-center">
                    <el-avatar :size="30" class="mr-12">
                        {{ (usermess.name && usermess.name.slice(-2)) || 'XX' }}
                    </el-avatar>
                    <el-button @click="loginOut" size="small" type="text">
                        退出登录
                    </el-button>
                </div>
            </div>
        </template>
        <router-view class="bg-default radius overflow-auto px-28 pb-20" style="margin-top: 0;" />
    </hc-frame>
</template>

<script>
import menuData from '../../router/menu';
export default {
    data() {
        return {
            collapse: false,
            menuData: menuData,
            frameConfig: {
                // 菜单展开宽度、菜单收起宽度
                width: { openMenu: 240, closeMenu: 60 },
                // 头部高度、菜单地图高度、页签高度
                height: { header: 50, footer: 0, tabs: 50, logo: 50 }
            },
            menuConfig: { label: 'name', icon: 'icon', children: 'children', value: 'code', parent: 'parent', fullPath: 'fullPath' }
        };
    },
    computed: {
        title() {
            return this.$route.meta.title;
        },
        usermess() {
            return this.$store.getters.getUsermess;
        }
    },
    methods: {
        loginOut(val) {
            this.$request.login.loginout().then(({ data }) => {
                this.$message.success('退出登录！');
                this.$store.commit('clearLocalMess');
                this.$router.push({ name: 'login' });
            });
        }
    }
};
</script>
`;
export default code;
