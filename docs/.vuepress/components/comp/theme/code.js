const code = {};
code.basic = `
<template>
    <hc-theme :functional="functionalValue" :text="textValue" :border="borderValue" :shadow="shadowValue" :other="otherValue">
        <el-select v-model="value" placeholder="请选择">
            <el-option
                v-for="item in options"
                :key="item.value"
                :label="item.label"
                :value="item.value">
            </el-option>
        </el-select>
    </hc-theme>
</template>

<script>
export default {
    name: "Basic",
    data() {
        return {
            options: [{
                value: '0',
                label: '颜色一'
            }, {
                value: '1',
                label: '颜色二'
            }],
            value: '0',
            functionalData: [
                { primary: "#FFA07A", success: "#FF7F50", info: "#FF4500", warning: "#E9967A", danger: "#8B4513" },
                { primary: "#00CED1", success: "#2F4F4F", info: "#008B8B", warning: "#008B8B", danger: "#20B2AA" },
            ],
            textValue: { primary: "#FFBE3F", regular: "rgb(0,0,0)", secondary: "#909399", placeholder: "#c0c4cc" },
            borderValue: { base: "#dcdfe6", light: "#e4e7ed", lighter: "#ebeef5", extraLight: "#f2f6fc" },
            shadowValue: "#000000",
            otherValue: { otherColor: "red" },
    },
    computed: {
        functionalValue() {
            return this.functionalData[this.value]
        },
    }
};
</script>

<style scoped>
</style>

`;
code.other = `
<template>
    <hc-theme :other="otherValue" class="mt-20">
        <div class="other">其他颜色</div>
    </hc-theme>
</template>

<script>
export default {
    data() {
        return {
            otherValue: {colorValue: '#3CB371' }
        }
    }
};
</script>

<style scoped>
.other {
    color: var(--color-other-colorValue);
}

</style>

`;
export default code;
