const code = {};
code.basic = `
<template>
    <el-form :model="form" size="small" label-width="120px" class="w-100">
        <hc-form-item :form="form" same-label :config="formConfig"></hc-form-item>
    </el-form>
</template>
<script>
export default {
    data() {
        return {
            form: {desc: [1] },
            formConfig: [
                { itemType: 'input', prop: 'name', label: '输入框' },
                { itemType: 'text', prop: 'name', label: '文本显示', formatter: form => form.name + '文本显示' },
                {
                    itemType: 'radio',
                    prop: 'region',
                    label: '单选框',
                    list: [{ label: '3333', value: 2 },{ label: '444', value: 1 }]
                },
                {
                    itemType: 'checkbox',
                    prop: 'desc',
                    label: '多选框',
                    list: [{ label: '3333', value: 2 }, { label: '444', value: 1 }]
                },
                {
                    itemType: 'select',
                    prop: 'resource',
                    label: '下拉框',
                    list: [{ label: '3333', value: 2 },{ label: '444', value: 1 }]
                },
                { itemType: 'switch', prop: 'date1', label: 'switch' },
                { itemType: 'date-picker', prop: 'date2', label: '日期选择' }
            ]
        }
    }
}
</script>
`;
code.rightSide = `
<template>
    <el-form :model="form" size="small" label-width="80px" class="w-100 mt-20">
        <hc-form-item :form="form" :config="formConfig" right-side>
            <el-button>重置</el-button>
            <el-button type="primary">查询</el-button>
        </hc-form-item>
    </el-form>
</template>
<script>
export default {
    data() {
        return {
            form: { name: '', region: '', date1: '', date2: '', slots: '', resource: '', desc: [1] },
            formConfig: [
                { itemType: 'input', prop: 'name', label: '输入框' },
                { itemType: 'input', prop: 'name', label: '输入框' },
                { itemType: 'input', prop: 'name', label: '输入框' },
                { itemType: 'input', prop: 'name', label: '输入框' },
                { itemType: 'date-picker', prop: 'date2', label: '日期选择' }
            ]
        }
    }
}
</script>

`;
code.column = `
<template>
    <el-form :model="form" size="small" class="w-100">
        <hc-form-item :form="form" same-label :column="3" :config="formConfig"></hc-form-item>
    </el-form>
</template>
<script>
export default {
    data() {
        return {
            form: { desc: [1] },
            formConfig: [
                { itemType: 'input', prop: 'name', label: '输入框' },
                { itemType: 'text', prop: 'name', label: '文本显示', formatter: form => form.name + '文本显示' },
                {
                    itemType: 'radio',
                    prop: 'region',
                    label: '单选框',
                    list: [{ label: '3333', value: 2 },{ label: '444', value: 1 }]
                },
                {
                    itemType: 'checkbox',
                    prop: 'desc',
                    label: '多选框',
                    list: [{ label: '3333', value: 2 }, { label: '444', value: 1 }]
                },
                { itemType: 'select', prop: 'resource', label: '下拉框', span: 2, list: [] },
                { itemType: 'switch', prop: 'date1', label: 'switch' },
                { itemType: 'date-picker', prop: 'date2', label: '日期选择' }
            ]
        }
    }
}
</script>
`;
code.slot = `
<template>
    <el-form :model="form" size="small" class="w-100">
        <hc-form-item :form="form" same-label :column="3" :config="formConfig">
            <template #slots="data">
                <el-input placeholder="请输入内容" v-model="form[data.prop]">
                    <template slot="prepend"
                    >Http://</template
                    >
                </el-input>
            </template>
        </hc-form-item>
    </el-form>
</template>
<script>
export default {
    data() {
        return {
            form: {},
            formConfig: [
                { itemType: 'input', prop: 'name', label: '输入框' },
                { itemType: 'input', prop: 'slots', label: '输入框22', isSlot: true },
                { itemType: 'text', prop: 'name', label: '文本显示', formatter: form => form.name + '文本显示' },
                { itemType: 'select', prop: 'resource', label: '下拉框', span: 2, list: [] },
                { itemType: 'date-picker', prop: 'date2', label: '日期选择' }
            ]
        }
    }
}
</script>
`;
code.other = `
<template>
    <el-form :model="form" :rules="rules" size="small" class="w-100">
        <hc-form-item :form="form" :rules="rules" disabled clearable same-label :column="3" :config="formConfig"></hc-form-item>
    </el-form>
</template>
<script>
export default {
    data() {
        return {
            form: {},
            formConfig: [
                { itemType: 'input', prop: 'name', label: '输入框' },
                { itemType: 'input', prop: 'name', label: '输入框2' },
                { itemType: 'text', prop: 'name', label: '文本显示', formatter: form => form.name + '文本显示' },
                { itemType: 'select', prop: 'resource', label: '下拉框', span: 2, list: [] },
                { itemType: 'switch', prop: 'date1', label: 'switch' },
                { itemType: 'date-picker', prop: 'date2', label: '日期选择' }
            ],
            rules: {
                name: [{ required: true, message: '请输入活动名称', trigger: 'blur' }],
            }
        }
    }
}
</script>
`;
code.demo = `
<template>
    <el-form :model="form" :rules="rules" size="small" class="w-100">
        <hc-form-item :form="form" :rules="rules" :config="formConfig" :column="3" width="220px" same-label>
            <template #slots="data">
                <el-input placeholder="请输入内容" v-model="form[data.prop]">
                    <template slot="prepend"
                    >Http://</template
                    >
                </el-input>
            </template>
        </hc-form-item>
    </el-form>
</template>
<script>
export default {
    data() {
        return {
            form: { date3: [], desc: [1] },
            formConfig: [
                { itemType: 'input', prop: 'name', disabled: true, label: '输入框' },
                { prop: 'name', isSlot: true, label: '输入框' },
                { itemType: 'text', prop: 'name', label: '文本显示', formatter: form => form.name || '' + '文本显示' },
                {
                    itemType: 'radio',
                    prop: 'region',
                    label: '单选框',
                    list: [
                        { label: '3333', value: 2 },
                        { label: '444', value: 1 }
                    ]
                },
                {
                    itemType: 'checkbox',
                    prop: 'desc',
                    label: '多选框',
                    list: [
                        { label: '3333', value: 2 },
                        { label: '444', value: 1 }
                    ]
                },
                { itemType: 'select', prop: 'resource', label: '下拉框', width: '100%', clearable: true, span: 2, list: [] },
                { itemType: 'switch', prop: 'date1', label: 'switch' },
                { itemType: 'date-picker', type: 'date', width: '300px', prop: 'date2', label: '日期选择', 'value-format': 'yy-MM-dd' },
                { itemType: 'date-picker', type: 'daterange', prop: 'date3', label: '日期选择', width: '100%', span: 2 }
            ],
            rules: {
                name: [{ required: true, message: '请输入活动名称', trigger: 'blur' }],
            }
        }
    }
}
</script>
`;
export default code;
