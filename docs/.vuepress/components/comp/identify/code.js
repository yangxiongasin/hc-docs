const code = {};
code.basic = `
<template>
    <hc-identify v-model="identify"></hc-identify>
</template>
<script>
export default {
    data() {
        return {
            identify: ''
        }
    }
}
</script>
`;
code.content = `
<template>
    <hc-identify v-model="identify" :composition="composition"></hc-identify>
</template>
<script>
export default {
    data() {
        return {
            identify: '',
            composition: '1234567890'
        }
    }
}
</script>
`;
code.demo = `
<template>
    <hc-identify v-model="identify" codeLength="6" :composition="composition" :width="200" :height="200"></hc-identify>
</template>
<script>
export default {
    data() {
        return {
            identify: '',
            composition: '1234567890'
        }
    }
}
</script>
`;
code.length = `
<template>
    <hc-identify v-model="identify" :width="200" :height="200"></hc-identify>
</template>
<script>
export default {
    data() {
        return {
            identify: ''
        }
    }
}
</script>
`;
code.wh = `
<template>
    <hc-identify v-model="identify"></hc-identify>
</template>
<script>
export default {
    data() {
        return {
            identify: ''
        }
    }
}
</script>
`;
export default code;
