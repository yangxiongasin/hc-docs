const code = {};
code.basic = `
<template>
    <hc-pager :total="total" :page="pager" @change="pageChange"></hc-pager>
</template>
<script>
export default {
    data() {
        return {
            total: 100,
            pager: { pageNo: 1, pageSize: 10 }
        }
    },
    methods: {
        pageChange(data) {
            this.pager = Object.assign(this.pager, data)
        }
    }
}
</script>
`;
code.layout = `
<template>
    <hc-pager :total="total" :page="pager" :layout="layout" @change="pageChange"></hc-pager>
</template>
<script>
export default {
    data() {
        return {
            layout: 'total, prev, pager, next',
            total: 100,
            pager: { pageNo: 1, pageSize: 10 }
        }
    },
    methods: {
        pageChange(data) {
            this.pager = Object.assign(this.pager, data)
        }
    }
}
</script>
`;
code.demo = `
<template>
    <hc-pager :total="total" :page="pager" @change="pageChange"></hc-pager>
</template>
<script>
export default {
    data() {
        return {
            total: 100,
            pager: { pageNo: 1, pageSize: 10 }
        }
    },
    methods: {
        pageChange(data) {
            this.pager = Object.assign(this.pager, data)
        }
    }
}
</script>
`;
export default code;
