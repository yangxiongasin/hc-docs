const code = {};
code.first = `
//引入图标
config.module.rule("svg").exclude.add(resolve("./src/assets/fonts/svg"));
config.module.rule("icon").test(/\\.svg$/)
    .include.add(resolve("./src/assets/fonts/svg")).end()
    .use("svg-sprite-loader")
    .loader("svg-sprite-loader")
    .options({
        symbolId:'icon-[name]'
    });
`;
code.second = `
const load = require.context("./svg",false,/\\.svg$/);
load.keys().map(load);
`;
code.user = `
<hc-icon name="对应svg文件名" type="success" size="600px"></hc-icon>
<hc-icon name="对应svg文件名" ></hc-icon>
`;
export default code;
