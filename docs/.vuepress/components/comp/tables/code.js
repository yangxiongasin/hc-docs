const code = {};
code.basic = `
<template>
    <hc-table :data="tableData" :column="tableColumn"></hc-table>
</template>

<script>
export default {
    data() {
        return {
            tableData: [
                { name: 'Bobo', sex: 1 },
                { name: 'Lucy', sex: 0 }
            ],
            tableColumn: [
                { label: '姓名', prop: 'name' },
                { label: '性别', prop: 'sex', formatter: row => ({ 0: '女', 1: '男' }[row.sex]) }
            ]
        }
    }
}
</script>
`;
code.checkbox = `
<template>
    <hc-table :data="tableData" :column="tableColumn" checkbox @on-select="getSelect"></hc-table>
</template>

<script>
export default {
    data() {
        return {
            tableData: [
                { name: 'Bobo', sex: 1 },
                { name: 'Lucy', sex: 0 }
            ],
            tableColumn: [
                { label: '姓名', prop: 'name' },
                { label: '性别', prop: 'sex', formatter: row => ({ 0: '女', 1: '男' }[row.sex]) }
            ],
            tableSelectData: []
        }
    },
    methods: {
        getSelect(data) {
            this.tableSelectData = data;
        }
    }
}
</script>
`;
code.cellConfig = `
<template>
    <hc-table :data="tableData" :column="tableColumn" :cell-config="cellConfig"></hc-table>
</template>

<script>
export default {
    data() {
        return {
            tableData: [
                { name: 'Bobo', sex: 1 },
                { name: 'Lucy', sex: 0 }
            ],
            tableColumn: [
                { label: '姓名', prop: 'name' },
                { label: '性别', prop: 'sex', formatter: row => ({ 0: '女', 1: '男' }[row.sex]) }
            ],
            cellConfig: {
                sex: { 1: 'color-danger', 0: 'color-primary' }
            }
        }
    }
}
</script>

`;
code.columnSlot = `
<template>
    <hc-table :data="tableData" :checkbox="false" :column="tableColumn" :showHeaders="['propNameId']">
        <!-- 默认插槽为操作列插槽， 可通过slot-scope获取当前行数据，可在插槽内部使用  -->
        <template #propNameId="{row, $index}">
            <el-button @click="clickId(row, $index)">
                click
            </el-button>
        </template>
        <template #propNameIdHeader="data">
            <div>自定义属性项ID<i class="el-icon-user-solid color-success" /></div>
        </template>
    </hc-table>
</template>

<script>
export default {
    data() {
        return {
            tableData: [
                { propNameId: 111, status: 0 },
                { propNameId: 123, status: 1 }
            ],
            // column： 对象内第一个键值对，key为column的prop值，value为column的label
            tableColumn: [
                { label: '属性项ID', prop: 'propNameId', isSlot: true },
                { label: '状态', prop: 'status', formatter: row => (row.status ? '启用' : '禁用') }
            ]
        }
    },
    methods: {
        clickId(row, index) {
            this.$message.info('当前行数据是' + JSON.stringify(row) + ';当前行数为：' + (index + 1))
        }
    }
}
</script>

`;
code.expand = `
<template>
    <hc-table :data="tableData" :column="tableColumn" expand>
        <template #expand="data">
            <div>这里可以放展开的内容</div>
        </template>
    </hc-table>
</template>

<script>
export default {
    data() {
        return {
            tableData: [
                { name: 'Bobo', sex: 1 },
                { name: 'Lucy', sex: 0 }
            ],
            tableColumn: [
                { label: '姓名', prop: 'name' },
                { label: '性别', prop: 'sex', formatter: row => ({ 0: '女', 1: '男' }[row.sex]) }
            ]
        }
    }
}
</script>
`;
code.operation = `
<template>
    <hc-table :data="tableData" :column="tableColumn" operation-width="80px">
        <template #default="{row, $index}">
            <el-button type="text" @click="onDelete(row, $index)">删除</el-button>
        </template>
    </hc-table>
</template>
<script>
    export default {
        data() {
            return {
                tableData: [{ propNameId: 111, status: 0 },
                    { propNameId: 123, status: 1 }],
                tableColumn: [
                    { label: '属性项ID', prop: 'propNameId' },
                    { label: '状态', prop: 'status', formatter: row => (row.status ? '启用' : '禁用') }
                ]
            }
        }
    }
</script>
`;
export default code;
