const code = {};
code.basic = `
<template>
    <hc-popover title="是否确认删除该数据？" @submit="onsubmit({name: '第一条数据'})">
        <el-button size="small">删除</el-button>
    </hc-popover>
</template>
<script>
export default {
    methods: {
        onsubmit(data) {
            this.$message.success(data.name + '删除成功！');
        }
    }
}
</script>
`;
code.config = `
<template>
    <hc-popover @submit="onsubmit({ name: '第一条数据' })" icon="el-icon-delete" class-name="color-danger" title="是否确认删除该数据？">
        <el-button size="small">
            删除
        </el-button>
    </hc-popover>
</template>
<script>
export default {
    methods: {
        onsubmit(data) {
            this.$message.success(data.name + '删除成功！');
        }
    }
}
</script>
`;
export default code;
