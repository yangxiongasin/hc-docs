const code = {};
code.basic = `
<template>
    <hc-dialog :outClose="outClose" title="213">
        <el-button>弹窗</el-button>
        <template #contain>
            <div>这是内容区域</div>
        </template>
    </hc-dialog>
</template>
<script>
export default {
    data() {
        return {
            outClose: false
        }
    }
}
</script>
`;
code.beforeOpen = `
<template>
    <hc-dialog :outClose="outClose" :before-open="beforeOpen" title="213">
        <el-button>弹窗</el-button>
        <template #contain>
            <div>这是内容区域</div>
        </template>
    </hc-dialog>
</template>
<script>
export default {
    data() {
        return {
            outClose: false
        }
    },
    methods: {
        beforeOpen(done) {
            setTimeout(() => {
                Math.random() > 0.5 ? done() : this.$message.error('当前随机值小于0.5, 不能打开弹窗!');
            }, 300)
        }
    }
}
</script>
`;
code.drag = `
<template>
    <hc-dialog :outClose="outClose" drag title="213">
        <el-button>弹窗</el-button>
        <template #contain>
            <div>这是内容区域</div>
        </template>
    </hc-dialog>
</template>
<script>
export default {
    data() {
        return {
            outClose: false
        }
    }
}
</script>
`;
export default code;
