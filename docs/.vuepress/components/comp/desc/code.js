const code = {};
code.basic = `
<template>
    <hc-desc :data="data" :config="infoConfig"></hc-desc>
</template>
<script>
export default {
    data() {
        return {
            infoConfig: [
                { label: '姓名', prop: 'name' },
                { label: '身份证', prop: 'IDCard', span: 2 },
                { label: '性别', prop: 'sex', formatter: data => ({ 0: '男', 1: '女' }[data.sex]) },
                { label: '手机号', prop: 'phone',code:'phoneCode' },
                { label: '身高', prop: 'height' },
                { label: '体重', prop: 'weight' }
            ],
            data: { name: '杨雄', sex: 0, IDCard: '430624******0000', phone: '13512341234', height: '248cm', weight: '10kg' }
        }
    }
}
</script>
`;
code.slot = `
<template>
    <hc-desc :data="data" :config="infoConfig">
        <template #sex="data">
             <el-button size="small" type="primary">
                 <i class="el-icon-s-custom"></i>
                 {{ data ? '男' : '女' }}
             </el-button>
        </template>
    </hc-desc>
</template>

<script>
export default {
    data() {
        return {
            infoConfig: [
                {
                    label: '姓名',
                    prop: 'name',
                    click: (value, data) => {
                        this.$message.warning(value + JSON.stringify(data))
                    }
                },
                { label: '身份证', prop: 'IDCard' },
                { label: '性别', prop: 'sex', isSlot: true },
                { label: '手机号', prop: 'phone' },
                { label: '身高', prop: 'height' },
                { label: '体重', prop: 'weight' }
            ],
            data: { name: '杨雄', sex: 0, IDCard: '430624******0000', phone: '13512341234', height: '248cm', weight: '10kg' }
        }
    }
}
</script>
`;
code.style = `
<template>
    <hc-desc :data="data" :config="infoConfig" :column="2" :distance="8" same-label :color="colorList" />
</template>

<script>
export default {
    data() {
        return {
            colorList: { label: "danger", value: "primary" },
            infoConfig: [
                {label: '姓名',prop: 'name' },
                { label: '身份证', prop: 'IDCard' },
                { label: '性别', prop: 'sex' },
                { label: '手机号', prop: 'phone' },
                { label: '身高', prop: 'height' },
                { label: '体重', prop: 'weight' }
            ],
            data: { name: '杨雄', sex: '男', IDCard: '430624******0000', phone: '13512341234', height: '248cm', weight: '10kg' }
        }
    }
}
</script>
`;
export default code;
