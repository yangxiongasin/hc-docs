const code = {};
code.mixins = `
export default {
    data() {
        return {
            form: {},
            tableData: [],
            pager: {
                pageNo: 1,
                pageSize: 10
            },
            total: 0
        };
    },
    methods: {
        // 点击查询时，恢复分页数据
        onSearch() {
            this.pager = {
                pageNo: 1,
                pageSize: 10
            };
            this.httpGetTable();
        },
        // page 组件变化情况
        pageChange(data) {
            this.pager = data;
            this.httpGetTable();
        }
    }
};
    `;
code.formTablePager = `
<template>
    <div class="m-20 border p-20 radius">
        <el-form ref="form" :model="form" :rules="rules" size="small">
            <hc-form-item :form="form" :config="formConfig" :rules="rules" sameLabel class="formButtonRight">
                <div>
                    <el-button @click="form = {checboxVal: []}">重置</el-button>
                    <el-button type="primary" @click="onSearch">搜索</el-button>
                </div>
            </hc-form-item>
        </el-form>
        <hc-table :data="tableData" :column="tableColumn" :cellConfig="cellConfig" show-operation operationWidth="140px" class="mt-20">
            <template>
                <div>
                    <el-button type="text">编辑</el-button>
                    <el-button type="text">详情</el-button>
                    <el-button type="text">删除</el-button>
                </div>
            </template>
        </hc-table>
        <hc-pager :total="total" :page="pager" background @change="pageChange" class="flex-x-end mt-20"></hc-pager>
    </div>
</template>

<script>
import searchPagerMixin from './searchPagerMixin';
export default {
    mixins: [searchPagerMixin],
    data() {
        return {
            form: { checboxVal: [] },
            rules: {},
            formConfig: [
                { itemType: 'input', prop: 'ipt', label: '普通输入框' },
                { itemType: 'input', type: 'int', prop: 'int', label: '纯数字输入框' },

                { itemType: 'date-picker', type: 'date', prop: 'date', label: '日期选择器' }
            ],
            tableColumn: [
                { label: '普通输入框', prop: 'ipt' },
                { label: '纯数字输入框', prop: 'int' },
                { label: '复选框', prop: 'checboxVal' },
                { label: '下拉框', prop: 'selectVal', formatter: row => ({ 1: 'A', 2: 'B' }[row.selectVal]) }
            ],
            cellConfig: {
                selectVal: { 2: 'color-danger', 1: 'color-success' }
            }
        };
    },
    created() {
        this.httpGetTable();
    },
    methods: {
        httpGetTable() {
            this.tableData = [
                { ipt: '测试数据' + parseInt(Math.random() * 100), int: parseInt(Math.random() * 100), selectVal: 1, checboxVal: [1, 2] },
                { ipt: '测试数据' + parseInt(Math.random() * 100), int: parseInt(Math.random() * 100), selectVal: 2, checboxVal: [1, 2] }
            ];
        }
    }
};
</script>
<style lang="scss">
    .formButtonRight .slot {
        display: flex;
        justify-content: flex-end;
    }
</style>
    `;
export default code;
