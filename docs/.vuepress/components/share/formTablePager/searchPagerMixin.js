export default {
    data() {
        return {
            form: {},
            tableData: [],
            pager: {
                pageNo: 1,
                pageSize: 10
            },
            total: 0
        };
    },
    methods: {
        // 点击查询时，恢复分页数据
        onSearch() {
            this.pager = {
                pageNo: 1,
                pageSize: 10
            };
            this.httpGetTable();
        },
        // page 组件变化情况
        pageChange(data) {
            this.pager = data;
            this.httpGetTable();
        }
    }
};
