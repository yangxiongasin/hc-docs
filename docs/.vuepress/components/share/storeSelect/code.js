const code = {};
code.all = `
import Vuex from 'vuex';
import Vue from 'vue';
import http from '../http';
import main from "../main";
Vue.use(Vuex);
Vue.use(http);

const store = new Vuex.Store({
    state: {
        select: {}
    },
    mutations: {
        setSelect(state, payload) {
            state.select[payload.code] = payload.data;
            state.select = {...state.select};
        }
    },
    actions: {
        getSelect(context, arr) {
            arr.forEach(e => {
                if (!context.state.select[e]) {
                    main.$http.post(main.$service.main.code, {
                        categoryCode: e
                    }).then(res => {
                        context.commit('setSelect', {code: e, data: res.data} );
                    });
                }
            });
        }
    },
    getters: {
        getSelectData: (state, getters) => (code) => {
            return state.select[code] || [];
        },
        getSelectDataName: (state, getters) => (code, value) => {
            const data = state.select[code] || []
            for (let i = 0; i < data.length; i++) {
                if (data[i].dictValue === value) {
                    return data[i].dictName
                }
            }
        }
    }
});

export default store;

`;
export default code;
