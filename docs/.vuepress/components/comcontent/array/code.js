const code = {};
code.filterOptionArray = `
<template>
    <div class="flex-x-between">
        <div class="col-8">{{ value }}</div>
        <div class="col-8">{{ changeValue }}</div>
        <div class="col-8">{{ changeMoreValue }}</div>
    </div>
</template>
<script>
export default {
    data() {
        return {
            value: [
                { name: 'yangxiong', sex: 18, id: 43062412313123 },
                { name: 'liuxinyu', sex: 20, id: 12345678987654 }
            ],
            changeValue: [],
            changeMoreValue: [],
        }
    },
    mounted() {
        this.changeValue = this.$Func.filterOptionArray(this.value, 'id');
        this.changeMoreValue = this.$Func.filterOptionArray(this.value, 'id,sex');
    }
}
</script>

`;
code.flatten = `
<template>
    <div>
        <div class="mt-4">{{ value }}</div>
        <div class="mt-4">{{ changeValue }}</div>
    </div>
</template>
<script>
export default {
    data() {
        return {
            value: [1, [2, [3, 4]], 5, [6]],
            changeValue: [],
        }
    },
    mounted() {
        this.changeValue = this.$Func.flatten(this.value);
    }
}
</script>

`;
code.getEleCount = `
<template>
    <div class="flex-x-between">
        <div class="flex-column col-6">
            原数组：
            <div class="mt-4">{{ value }}</div>
        </div>
        <div class="flex-column col-6">
            统计单个值：
            <div class="mt-4">{{ changeValue }}</div>
        </div>
        <div class="flex-column col-6">
            统计多个值：
            <div class="mt-4">{{ changeMoreValue }}</div>
        </div>
    </div>
</template>
<script>
export default {
    data() {
        return {
            value: [1, 2, 3, 4, 5, 6, 71, 2, 2, 3, 4, 1, 3, 2, 3],
            changeValue: [],
            changeMoreValue: []
        }
    },
    mounted() {
        this.changeValue = this.$Func.getEleCount(this.value, 2)
        this.changeMoreValue = this.$Func.getEleCount(this.value, [2, 3])
    }
}
</script>
`;
code.isArray = `
<template>
    <div class="flex-x-between">
        <div class="flex-column col-6">
            原值{{ value }}：
            <div class="mt-4">判定值：{{ $Func.isArray(value) }}</div>
        </div>
        <div class="flex-column col-6">
            原值{{ value2 }}：
            <div class="mt-4">判定值：{{ $Func.isArray(value2) }}</div>
        </div>
        <div class="flex-column col-6">
            原值{{ value3 }}：
            <div class="mt-4">判定值：{{ $Func.isArray(value3) }}</div>
        </div>
    </div>
</template>
<script>
export default {
    data() {
        return {
            value: [],
            value2: {},
            value3: 123,
        }
    },
}
</script>
`;
code.isEmptyArray = `
<template>
    <div class="flex-x-between">
        <div class="flex-column col-6">
            原值{{ value }}：
            <div class="mt-4">判定值：{{ $Func.isEmptyArray(value) }}</div>
        </div>
        <div class="flex-column col-6">
            原值{{ value2 }}：
            <div class="mt-4">判定值：{{ $Func.isEmptyArray(value2) }}</div>
        </div>
        <div class="flex-column col-6">
            原值{{ value3 }}：
            <div class="mt-4">判定值：{{ $Func.isEmptyArray(value3) }}</div>
        </div>
    </div>
</template>
<script>
export default {
    data() {
        return {
            value: [],
            value2: [1],
            value3: [{}],
        }
    },
}
</script>
`;
code.isEqual = `
<template>
    <div class="flex-x-between">
        <div class="flex-column col-6">
            数组1{{ value }}：
            数组2{{ value2 }}：
            <div class="mt-4">判定值：{{ $Func.isEqual(value, value3) }}</div>
        </div>
        <div class="flex-column col-6">
            数组1{{ value }}：
            数组2{{ value3 }}：
            <div class="mt-4">判定值：{{ $Func.isEqual(value, value3) }}</div>
        </div>
    </div>
</template>
<script>
export default {
    data() {
        return {
            value: [{a: 2}, 1],
            value2: [{a: 2}, 1],
            value3: [{a: 2, b: 3}, 1],
        }
    },
}
</script>

`;

code.repeatObjEle = `
<template>
    <div class="flex-x-between">
        <div class="flex-column col-6">
            原数组1：
            <div class="mt-4">{{ value }}</div>
            原数组2：
            <div class="mt-4">{{ value2 }}</div>
        </div>
        <div class="flex-column col-6">
            根据id字段去重的值：
            <div class="mt-4">{{ changeValue }}</div>
        </div>
    </div>
</template>
<script>
import Code from './code'
export default {
    data() {
        return {
            code: Code,
            value: [
                { name: 'yangxiong', sex: 18, id: 43062412313123 },
                { name: 'liuxinyu', sex: 20, id: 12345678987654 }
            ],
            value2: [
                { name: 'yangxiong', sex: 18, id: 43062412313123 },
                { name: 'liuxinyu2', sex: 22, id: 123123 }
            ],
            changeValue: [],
        }
    },
    mounted() {
        this.changeValue = this.$Func.repeatObjEle([...this.value, ...this.value2], 'id');
    }
}
</script>
`;

code.unSet = `
<template>
    <div class="flex-x-between">
        <div class="flex-column col-4">
            原数组1：
            <div class="mt-4">{{ value }}</div>
        </div>
        <div class="flex-column col-4">
            去重的值：
            <div class="mt-4">{{ changeValue }}</div>
        </div>
    </div>
</template>
<script>
export default {
    data() {
        return {
            value: [1,2,3,4,5,1,2,3,4,2],
            changeValue: [],
        }
    },
    mounted() {
        this.changeValue = this.$Func.unSet(this.value);
    }
}
</script>
`;
export default code;
