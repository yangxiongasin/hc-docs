const code = {};
code.errImg = `
<div class="flex-x-between text-18">
    <div class="flex-column">
        普通图片：<img class="col-8" src="" v-errImg />
    </div>
    <div class="flex-column">
        头像图片：<img class="col-24" src="" v-errImg="{ type: 'avatarErr' }" />
    </div>
    <div class="flex-column">
        上传图片：<img class="col-8" src="" v-errImg="{ type: 'upErr' }" />
    </div>
</div>
`;
code.preventShake = `
<el-button v-prevent-shake @click="$message.success('默认时间！')" class="col-3">默认时间</el-button>
<el-button v-prevent-shake:5000 @click="$message.success('5S后可点击！')" class="col-3">5S后可点击</el-button>
`;
code.copy = `
<template>
    <div class="d-flex text-18">
        <div v-copy="'当前数为' + count" class="d-flex">当前数为{{ count }}</div>
        <el-input v-model="countVal" class="col-8"></el-input>
    </div>
</template>
<script>
export default {
    data() {
        return {
            count: 0,
            countVal: '',
        }
    },
    mounted() {
        setInterval(() => {
            this.count++
        }, 4000)
    }
}
</script>

`;
export default code;
