const code = {};
code.deepMerge = `
<template>
    <div class="flex-x-between">
        <div class="flex-column col-6">
            原对象1：
            <div class="mt-4">{{ value }}</div>
        </div>
        <div class="flex-column col-6">
            原对象2：
            <div class="mt-4">{{ value }}</div>
        </div>
        <div class="flex-column col-6">
            合并后的对象：
            <div class="mt-4">{{ changeValue }}</div>
        </div>
    </div>
</template>
<script>
export default {
    data() {
        return {
            value: {a: 1, b: {a: 1, b: 1}},
            value2: {c: 3, b: {a: 1, d: 1}},
            changeValue: null
        }
    },
    mounted() {
        this.changeValue = this.$Func.deepMerge(this.value, this.value2);
    }
}
</script>
`;
export default code;
