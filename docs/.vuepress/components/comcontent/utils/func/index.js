import arrFunc from "./arr";
import browserCacheFunc from "./browserCache";
import dateFunc from "./date";
import objFunc from "./obj";
export default { ...arrFunc, ...browserCacheFunc, ...dateFunc, ...objFunc };
