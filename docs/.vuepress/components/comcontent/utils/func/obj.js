const deepMerge = (first, second) => {
    for (let key in second) {
        // 如果target(也就是first[key])存在，且是对象的话再去调用deepMerge，否则就是first[key]里面没这个对象，需要与second[key]合并
        first[key] = first[key] && first[key].toString() === "[object Object]" ? deepMerge(first[key], second[key]) : (first[key] = second[key]);
    }
    return first;
};
export default {
    deepMerge
};
