const code = {};
code.sass = `
$--color-default-hc: #ffffff; // 主题默认色
$--color-primary-hc: #1b86ee; // 主题色
$--color-success-hc: #40c073; // 主题成功色
$--color-info-hc: #8c8c8c; // 主题信息色
$--color-warning-hc: #faad14; // 主题警告色
$--color-danger-hc: #f5222d; // 主题错误色

$--color-text-primary-hc: #161d25; // 默认主题文本颜色
$--color-text-regular-hc: #454f5b; //
$--color-text-secondary-hc: #8c8c8c;
$--color-text-placeholder-hc: #bfbfbf; // 输入框内提示文本颜色
/*
 * 边框颜色不同亮度
 */
$--border-color-base-hc: #dcdfe6; //
$--border-color-light-hc: #e4e7ed;
$--border-color-lighter-hc: #ebeef5;
$--border-color-extra-light-hc: #f2f6fc;
// 边距基数
$sideBasic: 4; // padding、margin边距的基数值
$spaceMax: 21; // 边距基数的最大倍数
$--border-radius-base-hc: 4px; // 边框圆角基础值
$--background-color-base-hc: #f5f7fa; // 背景基础颜色值

$colMax: 25; // 24珊格col布局

// 文本字体大小和lineHeight的配合设置
$textList: (
(
    size: 12,
    line: 20
),
(
    size: 14,
    line: 20
),
(
    size: 16,
    line: 22
),
(
    size: 18,
    line: 24
),
(
    size: 20,
    line: 26
),
(
    size: 28,
    line: 40
),
(
    size: 36,
    line: 50
)
);
@import '~hc-basic/packages/scss/index';
$--font-path: '../../../../node_modules/element-ui/lib/theme-chalk/fonts';
@import '~element-ui/packages/theme-chalk/src/index'
    `;
code.config = `
export default {
    Components: {
        formItem: {
            labelFontSize: 14, // form项的label字体大小，用来设置label的宽度
            label: 'label', // 默认select、checkbox、radio的option的label字段
            value: 'value', // 默认select、checkbox、radio的option的value字段
            tCodeurl: '/code', // 使用tcode组件时，tcode后台地址
            space: '20px' // 各子组件中间距的大小
        },
        pager: {
            currentPage: 'pageNo', // 当前页配置字段
            pageSize: 'pageSize', // pager组件page-size字段
            layout: 'total, prev, pager, next, sizes, jumper' // pager组件layout
        },
        table: {
            noData: 'https://frontendonline-erp.ocj.com.cn/img/nodata.png', // 没有数据展示图片，可通过require导入本地图片
            optionLabel: '操作'
        },
        layout: {
            topHeight: 0, // 顶部内容高度
            bottomHeight: 0, // 菜单底部内容高度
            menuWidth: '240px', // 右侧菜单栏宽度
            closeMenuWidth: '64px' // 右侧菜单栏收起时宽度
        }
    },
    Encrypt: {
        AES: {
            KEY: '1234567812345678', // aes加密-密钥
            IV: '1234567812345678' // aes加密-密钥偏移量
        }
    },
    directives: {
        copy: {},
        errImg: {
            avatarErr: 'https://ocj-erp-frontendonline.oss-cn-shanghai.aliyuncs.com/img/avator.png', // 头像错误图片
            showErr: 'https://ocj-erp-frontendonline.oss-cn-shanghai.aliyuncs.com/img/nofound.png', // 展示错误图片
            upErr: 'https://ocj-erp-frontendonline.oss-cn-shanghai.aliyuncs.com/img/upload-fail.png' // 上传错误图片
        },
        onlyInt: {},
        onlyNumber: {},
        preventShake: {
            time: 2000
        }
    }
};
`;
code.eslint = `
module.exports = {
    root: true,
    env: {
        node: true,
        browser: true
    },
    extends: ['plugin:vue/recommended', 'eslint:recommended'],
    globals: {
        Vue: true,
        __webpack_public_path__: true
    },
    rules: {
        'vue/html-indent': [
            'error',
            4,
            {
                attribute: 1,
                alignAttributesVertically: true,
                ignores: []
            }
        ],
        'vue/max-attributes-per-line': [
            2,
            {
                singleline: 10,
                multiline: {
                    max: 1,
                    allowFirstLine: false
                }
            }
        ],
        // 属性链接，驼峰改 "-"
        "vue/attribute-hyphenation": ["error", "always" | "never", {
            "ignore": []
        }],
        // 此规则在单行元素的内容之前和之后强制换行, 默认打开，已关闭
        "vue/singleline-html-element-content-newline": [0, {
            "ignoreWhenNoAttributes": true,
            "ignoreWhenEmpty": true,
        }],
        "vue/no-dupe-keys": ["error", {
            "groups": []
        }],
        /**  属性顺序
         * DEFINITION e.g. 'is'
         * LIST_RENDERING e.g. 'v-for item in items'
         * CONDITIONALS e.g. 'v-if', 'v-else-if', 'v-else', 'v-show', 'v-cloak'
         * RENDER_MODIFIERS e.g. 'v-once', 'v-pre'
         * GLOBAL e.g. 'id'
         * UNIQUE e.g. 'ref', 'key', 'v-slot', 'slot'
         * TWO_WAY_BINDING e.g. 'v-model'
         * OTHER_DIRECTIVES e.g. 'v-custom-directive'
         * OTHER_ATTR e.g. 'custom-prop="foo"', 'v-bind:prop="foo"', ':prop="foo"'
         * EVENTS e.g. '@click="functionCall"', 'v-on="event"'
         * CONTENT e.g. 'v-text', 'v-html'
         */
        'vue/attributes-order': [
            'error',
            {
                order: ['LIST_RENDERING', 'CONDITIONALS', 'DEFINITION', 'RENDER_MODIFIERS', 'GLOBAL', 'UNIQUE', 'TWO_WAY_BINDING', 'OTHER_DIRECTIVES', 'OTHER_ATTR', 'EVENTS', 'CONTENT'],
                alphabetical: false
            }
        ],
        // template中不允许使用this
        "vue/this-in-template": ["error", "never"],
        // 实现自我关闭
        'vue/html-self-closing': [
            'error',
            {
                html: {
                    void: 'never',
                    normal: 'always',
                    component: 'always'
                },
                svg: 'always',
                math: 'always'
            }
        ],
        // 禁止在组件定义中使用保留名称
        "vue/no-reserved-component-names": ["error", {
            "disallowVueBuiltInComponents": true,
        }],
        // 规则配置 0：关闭，1：提示warring，2：error提示，
        'require-await': 2, // 不允许空async
        'no-compare-neg-zero': 2, //禁止与 -0 进行比较
        'no-alert': 0, //禁止使用alert confirm prompt
        'no-array-constructor': 2, //禁止使用数组构造器
        'no-bitwise': 0, //禁止使用按位运算符
        'no-caller': 1, //禁止使用arguments.caller或arguments.callee
        'no-catch-shadow': 2, //禁止catch子句参数与外部作用域变量同名
        'no-class-assign': 2, //禁止给类赋值
        'no-cond-assign': 2, //禁止在条件表达式中使用赋值语句
        'no-console': 0, //禁止使用console
        'no-const-assign': 2, //禁止修改const声明的变量
        'no-constant-condition': 2, //禁止在条件中使用常量表达式 if(true) if(1)
        'no-continue': 0, //禁止使用continue
        'no-control-regex': 2, //禁止在正则表达式中使用控制字符
        'no-debugger': 0, //禁止使用debugger
        'no-delete-var': 2, //不能对var声明的变量使用delete操作符
        'no-div-regex': 1, //不能使用看起来像除法的正则表达式/=foo/
        'no-dupe-keys': 2, //在创建对象字面量时不允许键重复 {a:1,a:1}
        'no-dupe-args': 2, //函数参数不能重复
        'no-duplicate-case': 2, //switch中的case标签不能重复
        'no-else-return': 2, //如果if语句里面有return,后面不能跟else语句
        'no-empty': 2, //块语句中的内容不能为空
        'no-empty-character-class': 2, //正则表达式中的[]内容不能为空
        'no-empty-label': 0, //禁止使用空label
        'no-eq-null': 0, //禁止对null使用==或!=运算符
        'no-eval': 0, //禁止使用eval
        'no-ex-assign': 2, //禁止给catch语句中的异常参数赋值
        'no-extend-native': 0, //禁止扩展native对象
        'no-extra-bind': 0, //禁止不必要的函数绑定
        'no-extra-boolean-cast': 2, //禁止不必要的bool转换
        'no-extra-parens': 0, //禁止非必要的括号
        'no-extra-semi': 2, //禁止多余的冒号
        'no-fallthrough': 1, //禁止switch穿透
        'no-floating-decimal': 1, //禁止省略浮点数中的0 .5 3.
        'no-func-assign': 2, //禁止重复的函数声明
        'no-implicit-coercion': 1, //禁止隐式转换
        'no-implied-eval': 0, //禁止使用隐式eval
        'no-inline-comments': 0, //禁止行内备注
        'no-inner-declarations': [2, 'functions'], //禁止在块语句中使用声明（变量或函数）
        'no-invalid-regexp': 2, //禁止无效的正则表达式
        'no-invalid-this': 0, //禁止无效的this，只能用在构造器，类，对象字面量
        'no-irregular-whitespace': 2, //不能有不规则的空格
        'no-iterator': 0, //禁止使用__iterator__ 属性
        'no-label-var': 0, //label名不能与var声明的变量名相同
        'no-labels': 0, //禁止标签声明
        'no-lone-blocks': 0, //禁止不必要的嵌套块
        'no-lonely-if': 1, //禁止else语句内只有if语句
        'no-loop-func': 0, //禁止在循环中使用函数（如果没有引用外部变量不形成闭包就可以）
        'no-mixed-requires': [0, false], //声明时不能混用声明类型
        'no-mixed-spaces-and-tabs': [2, false], //禁止混用tab和空格
        'linebreak-style': [0, 'windows'], //换行风格
        'no-multi-spaces': 1, //不能用多余的空格
        'no-multi-str': 0, //字符串不能用\\换行
        'no-multiple-empty-lines': [
            1,
            {
                max: 2
            }
        ], //空行最多不能超过2行
        'no-native-reassign': 0, //不能重写native对象
        'no-negated-in-lhs': 0, //in 操作符的左边不能有!
        'no-nested-ternary': 0, //禁止使用嵌套的三目运算
        'no-new': 0, //禁止在使用new构造一个实例后不赋值
        'no-new-func': 0, //禁止使用new Function
        'no-new-object': 0, //禁止使用new Object()
        'no-new-require': 0, //禁止使用new require
        'no-new-wrappers': 0, //禁止使用new创建包装实例，new String new Boolean new Number
        'no-obj-calls': 2, //不能调用内置的全局对象，比如Math() JSON()
        'no-octal': 2, //禁止使用八进制数字
        'no-octal-escape': 0, //禁止使用八进制转义序列
        'no-param-reassign': 0, //禁止给参数重新赋值
        'no-path-concat': 0, //node中不能使用__dirname或__filename做路径拼接
        'no-plusplus': 0, //禁止使用++，--
        'no-process-env': 0, //禁止使用process.env
        'no-process-exit': 0, //禁止使用process.exit()
        'no-proto': 0, //禁止使用__proto__属性
        'no-redeclare': 2, //禁止重复声明变量
        'no-regex-spaces': 2, //禁止在正则表达式字面量中使用多个空格 /foo bar/
        'no-restricted-modules': 0, //如果禁用了指定模块，使用就会报错
        'no-return-assign': 0, //return 语句中不能有赋值表达式
        'no-script-url': 0, //禁止使用javascript:void(0)
        'no-self-compare': 2, //不能比较自身
        'no-sequences': 0, //禁止使用逗号运算符
        'no-shadow': 0, //外部作用域中的变量不能与它所包含的作用域中的变量或参数同名
        'no-shadow-restricted-names': 0, //严格模式中规定的限制标识符不能作为声明时的变量名使用
        'no-spaced-func': 0, //函数调用时 函数名与()之间不能有空格
        'no-sparse-arrays': 2, //禁止稀疏数组， [1,,2]
        'no-sync': 0, //nodejs 禁止同步方法
        'no-ternary': 0, //禁止使用三目运算符
        'no-trailing-spaces': 1, //一行结束后面不要有空格
        'no-this-before-super': 0, //在调用super()之前不能使用this或super
        'no-throw-literal': 0, //禁止抛出字面量错误 throw "error";
        'no-undef': 1, //不能有未定义的变量
        'no-undef-init': 1, //变量初始化时不能直接给它赋值为undefined
        'no-undefined': 1, //不能使用undefined
        'no-unexpected-multiline': 0, //避免多行表达式
        'no-underscore-dangle': 1, //标识符不能以_开头或结尾
        'no-unneeded-ternary': 1, //禁止不必要的嵌套 var isYes = answer === 1 ? true : false;
        'no-unreachable': 2, //不能有无法执行的代码
        'no-unused-expressions': 0, //禁止无用的表达式
        'no-unused-vars': [
            0,
            {
                vars: 'all',
                args: 'after-used'
            }
        ], //不能有声明后未被使用的变量或参数
        'no-use-before-define': 0, //未定义前不能使用
        'no-useless-call': 0, //禁止不必要的call和apply
        'no-void': 0, //禁用void操作符
        'no-var': 0, //禁用var，用let和const代替
        'no-warning-comments': [
            0,
            {
                terms: ['todo', 'fixme', 'xxx'],
                location: 'start'
            }
        ], //不能有警告备注
        'no-with': 0, //禁用with

        'array-bracket-spacing': [1, 'never'], //是否允许非空数组里面有多余的空格
        'arrow-parens': 0, //箭头函数用小括号括起来
        'arrow-spacing': 0, //=>的前/后括号
        'accessor-pairs': 0, //在对象中使用getter/setter
        'block-scoped-var': 0, //块语句中使用var
        'brace-style': [1, '1tbs'], //大括号风格
        'callback-return': 1, //避免多次调用回调什么的
        camelcase: 2, //强制驼峰法命名
        'comma-dangle': [0, 'never'], //对象字面量项尾不能有逗号
        'comma-spacing': 0, //逗号前后的空格
        'comma-style': [0, 'last'], //逗号风格，换行时在行首还是行尾
        complexity: [0, 11], //循环复杂度
        'computed-property-spacing': [0, 'never'], //是否允许计算后的键名什么的
        'consistent-return': 0, //return 后面是否允许省略
        'consistent-this': [2, 'that'], //this别名
        'constructor-super': 0, //非派生类不能调用super，派生类必须调用super
        curly: 0, //必须使用 if(){} 中的{}
        'default-case': 0, //switch语句最后必须有default
        'dot-location': 0, //对象访问符的位置，换行的时候在行首还是行尾
        'dot-notation': [
            0,
            {
                allowKeywords: true
            }
        ], //避免不必要的方括号
        'eol-last': 0, //文件以单一的换行符结束
        eqeqeq: 2, //必须使用全等
        'func-names': 0, //函数表达式必须有名字
        'func-style': [0, 'declaration'], //函数风格，规定只能使用函数声明/函数表达式
        'generator-star-spacing': 0, //生成器函数*的前后空格
        'guard-for-in': 0, //for in循环要用if语句过滤
        'handle-callback-err': 0, //nodejs 处理错误
        'id-length': 0, //变量名长度
        indent: [2, 4], //缩进风格
        'init-declarations': 0, //声明时必须赋初值
        'key-spacing': [
            0,
            {
                beforeColon: false,
                afterColon: true
            }
        ], //对象字面量中冒号的前后空格
        'lines-around-comment': 0, //行前/行后备注
        'max-depth': [0, 4], //嵌套块深度
        'max-lines': [
            2,
            {
                max: 500,
                skipBlankLines: true,
                skipComments: true
            }
        ], //代码行数不能超过200行（去除空格行跟注释）
        'max-len': [2, 200, 4], //字符串最大长度
        'max-nested-callbacks': [0, 2], //回调嵌套深度
        'max-params': [2, 5], //函数最多只能有5个参数
        'max-statements': [0, 10], //函数内最多有几个声明
        'new-cap': 2, //函数名首行大写必须使用new方式调用，首行小写必须用不带new方式调用
        'new-parens': 2, //new时必须加小括号
        'newline-after-var': 0, //变量声明后是否需要空一行
        'object-curly-spacing': [0, 'never'], //大括号内是否允许不必要的空格
        'object-shorthand': 0, //强制对象字面量缩写语法
        'one-var': 0, //连续声明
        'operator-assignment': [0, 'always'], //赋值运算符 += -=什么的
        'operator-linebreak': [0, 'after'], //换行时运算符在行尾还是行首
        'padded-blocks': 0, //块语句内行首行尾是否要空行
        'prefer-const': 0, //首选const
        'prefer-spread': 0, //首选展开运算
        'prefer-reflect': 0, //首选Reflect的方法
        quotes: [0, 'single'], //引号类型 \`\` "" ''
        'quote-props': [0, 'always'], //对象字面量中的属性名是否强制双引号
        radix: 0, //parseInt必须指定第二个参数
        'id-match': 0, //命名检测
        'require-yield': 0, //生成器函数必须有yield
        semi: [2, 'always'], //语句强制分号结尾
        'semi-spacing': [
            0,
            {
                before: false,
                after: true
            }
        ], //分号前后空格
        'sort-vars': 0, //变量声明时排序
        'space-after-keywords': [0, 'always'], //关键字后面是否要空一格
        'space-before-blocks': [0, 'always'], //不以新行开始的块{前面要不要有空格
        'space-before-function-paren': [0, 'always'], //函数定义时括号前面要不要有空格
        'space-in-parens': [0, 'never'], //小括号里面要不要有空格
        'space-infix-ops': 0, //中缀操作符周围要不要有空格
        'space-return-throw-case': 0, //return throw case后面要不要加空格
        'space-unary-ops': [
            0,
            {
                words: true,
                nonwords: false
            }
        ], //一元运算符的前/后要不要加空格
        'spaced-comment': 0, //注释风格要不要有空格什么的
        strict: 0, //使用严格模式
        'use-isnan': 2, //禁止比较时使用NaN，只能用isNaN()
        'valid-jsdoc': 0, //jsdoc规则
        'valid-typeof': 2, //必须使用合法的typeof的值
        'vars-on-top': 0, //var必须放在作用域顶部
        'wrap-iife': [0, 'inside'], //立即执行函数表达式的小括号风格
        'wrap-regex': 0, //正则表达式字面量用小括号包起来
        yoda: [0, 'never'], //禁止尤达条件
        globals: {
            // 内置的全局变量
            Vue: true
        }
    },
    parser: 'vue-eslint-parser',
    parserOptions: {
        ecmaVersion: 7,
        sourceType: 'module',
        parser: 'babel-eslint'
    },
    settings: {
        'import/resolver': 'webpack'
    }
}
`;
export default code;
