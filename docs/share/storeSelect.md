# Select Store 应用

:::tip
<br>
<span class="font-weight-bold">使用场景：</span> 当后端没有提供 Tcode，下拉框的数据需要通过接口调用<br/><br/>
<span class="font-weight-bold">使用目的：</span> 便于下拉框数据整体管理，不同页面使用相同下拉框无需多次调用<br/><br/>
<span class="font-weight-bold">技术点：</span> 主要使用全局 VueX<br/><br/>
:::

## State

<share-storeSelect-state></share-storeSelect-state>

## Mutation

<share-storeSelect-mutation></share-storeSelect-mutation>

## Action

<share-storeSelect-action></share-storeSelect-action>

## Getter

<share-storeSelect-getter></share-storeSelect-getter>

## 代码展示

<share-storeSelect-all></share-storeSelect-all>
