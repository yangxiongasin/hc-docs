# 说明

:::tip
<br>
<span class="font-weight-bold">使用背景：</span> 项目体量较大，考虑后期维护成本增加。在项目开始初期通过功能划分进行子应用拆分<br/><br/>
<span class="font-weight-bold">使用目的：</span> 子项目独立开发、独立部署互不干扰<br/><br/>
<span class="font-weight-bold">技术点：</span>qiankun、webpack<br/><br/>
<span class="font-weight-bold">情景模拟：</span>假设项目被拆分成主项目"main"、子项目"firstchild"、子项目"secondchild"<br/><br/>
:::
