# 子项目配置

### 1.vue 构造函数改造

```js
// 改造前
new Vue({
    router,
    store,
    render: (h) => h(App)
}).$mount("#app");

// 改造之后
let instance = null;
function render(props = {}) {
    const { container } = props;
    instance = new Vue({
        router,
        store,
        render: (h) => h(App)
    }).$mount(container ? container.querySelector("#app") : "#app");
}
```

### 2.保障子应用可以单独打开运行

> <div style="line-height: 28px; font-size: 16px">1) 当该项目没有挂载到qiankun主项目时，window全局对象中"__POWERED_BY_QIANKUN__"不存在。执行render()函数，运行对应到子应用</div>

```js
window.__POWERED_BY_QIANKUN__ ? (__webpack_public_path__ = window.__INJECTED_PUBLIC_PATH_BY_QIANKUN__) : render();
```

### 3.导出对应生命周期

> <div style="line-height: 28px; font-size: 16px">1）bootstrap: 只会在微应用初始化的时候调用一次，下次微应用重新进入时会直接调用 mount 钩子，不会再重复触发 bootstrap。通常我们可以在这里做一些全局变量的初始化，比如不会在 unmount 阶段被销毁的应用级别的缓存等 <br></div>
> <div style="line-height: 28px; font-size: 16px">2）mount: 应用每次进入都会调用 mount 方法，通常我们在这里触发应用的渲染方法<br></div>
> <div style="line-height: 28px; font-size: 16px">3）unmount: 应用每次 切出/卸载 会调用的方法，通常在这里我们会卸载微应用的应用实例</div>

```js
export async function bootstrap() {
    console.log("[vue] vue app bootstraped");
}

export async function mount(props) {
    render(props);
}

export async function unmount() {
    instance.$destroy();
    instance.$el.innerHTML = "";
    instance = null;
}
```

### 4.路由改造

> <div style="line-height: 28px; font-size: 16px">1）router.js: 将base地址设置为子项目名称（firstchild或者secondchild）作为前缀<br></div>

```js
const router = new VueRouter({
    mode: "history",
    base: "/firstchild",
    routes
});
```

> 2. 推荐在子项目中通过路由配置 path 路径中添加"/firstchild"前缀，而不是直接给 base 值

### 5.package.json 更改

> <div style="line-height: 28px; font-size: 16px">1）将package.json字段中name的值修改为对应的子项目名称<br></div>

### 6.webpack 配置

> <div style="line-height: 28px; font-size: 16px">1）以下是vue-cli3中vue.config.js配置<br></div>
> <div style="line-height: 28px; font-size: 16px">2）publicPath配置用于打包部署nginx后对应子应用静态文件获取<br></div>
> <div style="line-height: 28px; font-size: 16px">3）chainWebpack中针对于fonts和images配置项,主要用来处理当子项目嵌入到父项目中时。写在样式中静态文件引入（字体文件和背景图片）丢失的bug<br></div>
> <div style="line-height: 28px; font-size: 16px">4）configureWebpack中output配置主要用来：把库暴露给前使用的模块定义系统<br></div>
> <div style="line-height: 28px; font-size: 16px">5）devServer中的headers请求头允许跨域，因为主应用调取子应用静态文件通过fetch请求<br></div>

```js
const path = require("path");
const webpack = require("webpack");
const CompressionWebpackPlugin = require("compression-webpack-plugin");
const productionGzipExtensions = ["js", "css"];
const isProduction = process.env.NODE_ENV === "production";
const resolve = require("path").resolve;
const { name } = require("./package");
module.exports = {
    // whether to use eslint-loader 是否开启eslint
    lintOnSave: !isProduction,
    productionSourceMap: !isProduction,
    publicPath: isProduction ? "./" : "/",
    outputDir: "dist",
    assetsDir: "static",
    chainWebpack: (config) => {
        config.resolve.alias
            .set("@components", path.resolve(__dirname, "src/components")) //组件目录
            .set("@views", path.resolve(__dirname, "src/views")) //页面目录
            .set("@assets", path.resolve(__dirname, "src/assets")) //资源文件目录
            .set("@style", path.resolve(__dirname, "src/style")); //样式文件目录
        config.resolve.alias.delete("vue$"); //取消本地vue依赖
        //引入图标
        config.module.rule("svg").exclude.add(resolve("./src/assets/fonts/svg"));
        config.module
            .rule("icon")
            .test(/\.svg$/)
            .include.add(resolve("./src/assets/fonts/svg"))
            .end()
            .use("svg-sprite-loader")
            .loader("svg-sprite-loader")
            .options({
                symbolId: "icon-[name]"
            });
        config.module
            .rule("fonts")
            .test(/\.(woff2?|eot|ttf|otf)(\?.*)?$/)
            .use("url-loader")
            .loader("url-loader")
            .options({
                limit: 4096,
                name: "static/fonts/[name].[hash:8].[ext]",
                publicPath: process.env.NODE_ENV === "production" ? "/firstchild" : "//localhost:7071/"
            });

        config.module
            .rule("images")
            .test(/\.(png|jpe?g|gif|webp)(\?.*)?$/)
            .use("url-loader")
            .loader("url-loader")
            .options({
                limit: 4096,
                // name 代表 url-loader 会将资源写到 static/fonts/ 下
                name: "static/img/[name].[hash:8].[ext]",
                // publicPath 代表资源引入 url 会生成 /live 的前缀，比如 /live/static/img/bg_header.790a94f4.png
                publicPath: process.env.NODE_ENV === "production" ? "/firstchild" : "//localhost:7071/"
            });
    },
    configureWebpack: {
        output: {
            library: `${name}-[name]`,
            libraryTarget: "umd",
            jsonpFunction: `webpackJsonp_${name}`
        },
        plugins: [
            // Ignore all locale files of moment.js
            new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),

            // 配置compression-webpack-plugin压缩
            new CompressionWebpackPlugin({
                algorithm: "gzip",
                test: new RegExp("\\.(" + productionGzipExtensions.join("|") + ")$"),
                threshold: 10240,
                minRatio: 0.8
            }),
            new webpack.optimize.LimitChunkCountPlugin({
                maxChunks: 5,
                minChunkSize: 100
            })
        ]
    },
    devServer: {
        host: "0.0.0.0",
        port: "7071",
        disableHostCheck: true,
        https: false,
        hotOnly: false,
        hot: true,
        overlay: {
            warnings: false,
            errors: true
        },
        headers: {
            "Access-Control-Allow-Origin": "*"
        }
    },
    transpileDependencies: ["hc-basic"]
};
```
