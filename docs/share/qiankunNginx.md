# nginx 配置

```
server {
    listen  8081;
    server_name localhost;
    location / {
        root   主项目地址;
        index index.html;
        try_files $uri $uri/ /index.html;
    }
    location /firstchild {
        alias 第一个子项目地址;
        try_files $uri $uri/ /index.html;
        add_header Access-Control-Allow-Origin *;
        add_header Access-Control-Allow-Methods 'GET, POST, OPTIONS';
        add_header Access-Control-Allow-Headers 'DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization';
    }
    location /secondchild {
        alias 第二个子项目地址;
        try_files $uri $uri/ /index.html;
        add_header Access-Control-Allow-Origin *;
        add_header Access-Control-Allow-Methods 'GET, POST, OPTIONS';
        add_header Access-Control-Allow-Headers 'DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization';
    }
    // 所有的项目中vue.config.js中代理的接口请求地址都需要在这里写好
    location ^~ /industry/{
        proxy_pass  http://123.57.131.199:8090;
    }
    location ^~ /api/{
        proxy_pass  http://192.168.3.57:7880;
    }
}
```
