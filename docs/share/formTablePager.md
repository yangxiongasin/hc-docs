# form-table-pager 经典组合

## mixins 抽离

<share-formTablePager-mixins></share-formTablePager-mixins>

## 示例

<share-formTablePager-index></share-formTablePager-index>

## 代码分层

<share-formTablePager-codeLayer></share-formTablePager-codeLayer>
