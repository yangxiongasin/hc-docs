# main 项目配置

### 1.安装 qiankun

> （只需要在主项目中安装 qiankun，子项目中不需要）；

```js
npm i qiankun -S
```

### 2.在主项目 main 中注册子项目

> <div style="line-height: 28px; font-size: 16px">1）子项目名分别为firstchild和secondchild <br></div>
> <div style="line-height: 28px; font-size: 16px">2）在需要挂在子应用的vue文件中，在需要挂载子应用的位置添加一个带ID的div元素（文档中采用container）<br></div>
> <div style="line-height: 28px; font-size: 16px">3）父应用中需要进行子应用路径跳转，path路径需要加上对应的子项目路由的basicUrl</div>

```js
import { registerMicroApps, start, setDefaultMountApp } from "qiankun"; // 引入qiankun包中registerMicroApps、start、setDefaultMountApp方法

registerMicroApps([
    {
        name: "firstchild", // 子项目名称
        entry: "//localhost:7001", // 子项目中定义的port号
        container: "#container", // 挂载的容器，采用对应文件中的container的ID dom元素
        activeRule: "/firstchild" // 激活对应子项目的路由规则，该处代表当监听到路由中带有"/firstchild"字符时，挂载firstchild子应用
    },
    {
        name: "secondchild",
        entry: "//localhost:7002",
        container: "#container",
        activeRule: "/secondchild"
    }
]);

start();
setDefaultMountApp("/firstchild/子页面路由地址"); // 设置主应用启动后默认进入的微应用。
```
