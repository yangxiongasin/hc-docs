# qiankun 小技巧

## 主项目 VueX 全局共享

> vuex 为全局状态管理器，只需要在主项目中添加一个。子项目通过主项目 props 透传进去

> 主项目透传

```js
const props = { router: this.$router, store: this.$store, components: components };
const isProduction = process.env.NODE_ENV === "production";
const container = "#container";
registerMicroApps([
    { name: "vicesystem", entry: isProduction ? "/vicesystem/" : "//localhost:7071", activeRule: "/vicesystem/", container, props },
    { name: "viceproject", entry: isProduction ? "/viceproject/" : "//localhost:7072", activeRule: "/viceproject/", container, props }
]);
```

> 子项目接收

```js
function render(props = {}) {
    const { container } = props;
    // 将父项目传递的store通过Vue.observable报一遍，保持其响应式
    const store = Vue.observable(props.store);
    Vue.prototype.$request = request(store.state.request);
    Vue.use(props.components);
    instance = new Vue({
        router,
        // 注册store时使用的就是父项目的store
        store,
        render: (h) => h(App)
    }).$mount(container ? container.querySelector("#app") : "#app");
}
```

## 主项目 componets 全局共享

> 1.子项目通过主项目 props 透传进去

> 2.在子项目中使用父项目传递的 props.componets；使用 Vue.use(props.componets)

## 主项目 axios 全局共享

> 1.将 axios 请求封装后存储到 store 里面

> 2.在子项目中使用父项目传递到 store 中到请求方法进行 axios 请求
