# vue-cli3 + nginx 多项目部署

:::tip
记录一次在 nginx 同一端口下部署多个 vue 应用的过程。nginx 用来做代理很好用，可以解决跨域问题
:::

## 开发及部署环境

-   vue+vuecli3+nginx

## 前端配置

1. 为每一个项目约定一个路由（一个独属于项目的 baseUrl）, // 假定项目一为 demo1，项目二为 demo2

2. 在 vue.config.js 文件中找到 publicPath 配置，添加如下配置

```js
publicPath = process.env.NODE_ENV === "production" ? "/demo1/" : "/";
```

3. 在模版文件下添加 base

```html
<meta base=/demo1/ >
```

4. 将设定的 baseUrl 应用到 vue 的路由项目中去

```js
const router = new VueRouter({
    mode: "history",
    base: process.env.NODE_ENV === "production" ? "/demo1/" : "/",
    routes
});
```

5. 将项目二按照 2 ～ 4 步骤改为 demo2

## nginx 配置

```nginx
server {
    listen       4002;
    server_name  localhost;
    # root   这里存放两个项目的上级地址
    location / {
            root   demo1项目地址！;
            try_files $uri $uri/ @router;
            index  index.html;
        }
    location /demo2 {
        alias   demo2项目地址！;
        try_files $uri $uri/ /demo2/index.html;
        index  index.html;
    }
    location @router {
            rewrite ^.*$ /index.html last;
    }

    location ^~ /integrationPlatform/{
        proxy_pass  http://代理后端接口地址;
    }
    location ^~ /admin/{
        proxy_pass  http://代理后端接口地址;
    }
    location ^~ /web/{
        proxy_pass  http://代理后端接口地址;
    }
}
```

> #root 后面的“demo1 项目地址”、“demo2 项目地址”项目分别在不同的文件夹里，它们所在位置不重要，重要的是 nginx 可以找到并且发布出来。

> “demo1 项目地址”文件夹的地址前缀是 root，“demo2 项目地址”地址前缀是 alias

## root、alias

> root 与 alias 主要作用就是解释 location 后面的 uri，root 意思就是根据 root 路径+locationn 路径找到资源；alias 意思就是用 alias 路径替换对应的 location 路径内容。
