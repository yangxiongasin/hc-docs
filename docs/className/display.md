# Display 显示方式

---

> 隐藏元素

```css
.d-none {
    display: none;
}
```

> 以单元格方式显示

```css
.d-table-cell {
    display: table-cell;
}
```

> 以弹性盒子方式显示

```css
.d-flex {
    display: flex;
}
```

> 以块级元素方式显示

```css
.d-block {
    display: block;
}
```

> 以行内元素方式显示

```css
.d-inline {
    display: inline;
}
```

> 以行内块元素方式显示

```css
.d-inline-block {
    display: inline-block;
}
```
