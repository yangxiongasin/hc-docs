# Position 位置

---

## ClassName

#### relative: 相对定位

```css
.relative {
    position: relative;
}
```

#### absolute: 绝对定位

```css
.absolute {
    position: absolute;
}
```

#### fixed: 固定定位

```css
.fixed {
    position: fixed;
}
```

#### absolute-all 占据父元素宽高的 100%

```css
.absolute-all {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
}
```

#### absolute-y 占据父元素高度的 100%

```
.absolute-y {
	position: absolute;
	top: 0;
	bottom: 0;
}
```
