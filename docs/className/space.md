# Space 边距

---

## 特性

> 1.边距类为向一个元素分配 margin 或者 padding 属性; <br /> 2.支持单个方向、所有方向，以及垂直方向和水平方向。<br /> 3.使用{property}{sides}-{size}格式命名;<br /> 4.项目中使用的边距设计基数为 4

#### {property} 属性

> 1.m 为 margin; <br />
> 2.p 为 padding

#### {sides} 方向

> 1.t 为 margin-top 或者 padding-top; <br />
> 2.b 为 margin-bottom 或者 padding-bottom; <br />
> 3.l 为 margin-left 或者 padding-left; <br />
> 4.r 为 margin-right 或者 padding-right; <br />
> 5.x 为水平方向 margin-left 和 margin-right 或者 padding-left 和 padding-right; <br />
> 6.y 为垂直方向 margin-top 和 margin-bottom 或者 padding-top 和 padding-bottom; <br /> 7.空为 margin 或者 padding; <br />

#### {size} 大小

> {size}以 4 为基数的倍数，从 0px 到 60px

## 示例 {property}{sides}-{size}

<class-space-index></class-space-index>
