# 使用指南

## 介绍

> hc-basic 是一个基于 Vue + ElementUI 编写的一个项目内容公共包

> hc-basic 中包含了公共基础样式封装

> 文档中样式可以通过点击进行名称复制

-   内部包含了 block 和 Text 两个模块内容
    -   Block
        -   Border 边框
        -   Cursor 光标类型
        -   Display 显示方式
        -   flex 弹性盒子布局
        -   Float 浮动
        -   Position 位置
        -   Space 边距
            — Shadow 阴影
    -   Text
        -   Typography 文字
        -   Background 背景

## 使用

> 内部定义的 className 可以在项目全局使用

#### 项目中使用

```html
<div class="mt-4 flex-y-center"></div>
```
