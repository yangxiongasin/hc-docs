# 其它公共样式

---

## 字重

<class-other-index :data="[
{ class: 'fw-400', details: '字重值为400' },
{ class: 'fw-500', details: '字重值为500' },
{ class: 'fw-bold', details: '字重值为Blod' },
{ class: 'fw-none', details: '清除字重值' },
]"></class-other-index>

## 溢出处理

<class-other-index :data="[
{ class: 'overflow-auto', details: '内容溢出自适应' },
{ class: 'overflow-hidden', details: '内容溢出隐藏' },
{ class: 'white-space-nowrap', details: '文字不换行' },
{ class: 'text-ellipsis', details: '文字超出一行隐藏' },
{ class: 'text-ellipsis-2', details: '文字超出2行隐藏' },
{ class: 'text-ellipsis-3', details: '文字超出3行隐藏' },
]"></class-other-index>

## 盒子模型

<class-other-index :data="[
{ class: 'border-box', details: '将容器设置为盒子模型' },
]"></class-other-index>
