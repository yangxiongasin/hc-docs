---
custom-page-class
---

# Border 边框

---

## 说明

<div class="mt-20 color-text-regular text-14">
    <div class="color-text-primary font-weight-bold text-16">border 样式</div>
    <div class="ml-12 mt-12">
        <div>sides变量: top right bottom left;</div>
        <div>提供了border（外边框）、border-none（无边框）</div>
        <div>提供了border-${sides} 单边框</div>
        <div>提供了border-${sides}-0 对应单边框为空</div>
        <div>支持light、 lighter、 extra-light三种颜色的边框 变量为color</div>
        <div>命名规格为 border-${sides}-${color}、border-${sides}-${color}-0</div>
    </div>
</div>
<div class="mt-28 color-text-regular text-14">
    <div class="color-text-primary font-weight-bold text-16">radius 样式</div>
    <div class="ml-12 mt-12">
        <div>提供了radius-0(圆角为空)、radius-2、radius、radius-circle</div>
        <div>提供了radius-${sides} 单边圆角</div>
    </div>
</div>

-
-

## 边框示例

#### 外边框、无边框

<class-border-index type="border" :data="[
    { class: 'border', details: '外边框' },
    { class: 'border-none', details: '无边框' }
]"></class-border-index>

#### 单边框

<class-border-index type="border" :data="[
    { class: 'border-top', details: '上边框' },
    { class: 'border-right', details: '右边框' },
    { class: 'border-bottom', details: '下边框' },
    { class: 'border-left', details: '左边框' },
]"></class-border-index>

#### 单边为空边框

<class-border-index type="border" :data="[
    { class: 'border-top-none', details: '上边框为none' },
    { class: 'border-right-none', details: '右边框为none' },
    { class: 'border-bottom-none', details: '下边框为none' },
    { class: 'border-left-none', details: '左边框为none' }
]"></class-border-index>

## 圆角示例

#### 常用圆角

<class-border-index :data="[
    { class: 'radius-none', details: '清除圆角' },
    { class: 'radius-2', details: '2px圆角' },
    { class: 'radius', details: '全圆角' },
    { class: 'radius-circle', details: '圆角为50%' },
]"></class-border-index>

#### 单边圆角

<class-border-index :data="[
    { class: 'radius-top', details: '顶部圆角' },
    { class: 'radius-right', details: '右边圆角' },
    { class: 'radius-bottom', details: '底部圆角' },
    { class: 'radius-left', details: '左边圆角' }
]"></class-border-index>
