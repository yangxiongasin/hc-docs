# Float 浮动

---

## CLassName

> 左浮动

```css
.float-left {
    float: left;
}
```

> 右浮动

```css
.float-right {
    float: right;
}
```

> 覆盖原本的浮动,让元素不浮动;

```css
.float-none {
    float: none;
}
```

> 清除浮动;

```css
.clearfix {
    clear: both;
}
```

## 代码示例

<br/>
<div class="col-24 text-20" style="color: #fff; background: #87BEFF">
    <div class="border-box flex-center bg-primary float-left">float-left</div>
    <div class="border-box flex-center bg-primary float-right">float-right</div>
    <div class="clearfix"></div>
</div>

```html
<div class="col-24 text-20" style="color: #fff; background: #87BEFF">
    <div class="border-box flex-center bg-primary float-left">float-left</div>
    <div class="border-box flex-center bg-primary float-right">float-right</div>
    <div class="clearfix"></div>
</div>
```
