const isProduction = process.env.NODE_ENV === 'production';
module.exports = {
    // whether to use eslint-loader 是否开启eslint
    lintOnSave: !isProduction,
    productionSourceMap: !isProduction,
    publicPath: isProduction ? '/' : '/',
    outputDir: '../package/docs/dist',
    assetsDir: 'static',
};
